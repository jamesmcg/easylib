/*
Copyright 2014 James E McGuire

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

TintHorizProgressBar
*/

package com.jamesmcg.easylib.ui;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.PorterDuff;
import android.util.AttributeSet;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.jamesmcg.easylib.R;

/**
 * Created by James on 22/08/2014.
 */
public class TintHorizProgressBar extends ProgressBar
{
    public TintHorizProgressBar(Context context)
    {
        super(context);
        construct();
    }

    public TintHorizProgressBar(Context context, AttributeSet attrs)
    {
        super(context, attrs, android.R.attr.progressBarStyleHorizontal);
        construct(attrs);
    }

    public TintHorizProgressBar(Context context, AttributeSet attrs, int defStyle)
    {
        super(context, attrs, defStyle);
        construct(attrs);
    }

    private void construct()
    {
        setIndeterminate(true);
    }

    private void construct(AttributeSet attrs)
    {
        construct();

        TypedArray a = getContext().obtainStyledAttributes(attrs, R.styleable.TintHorizProgressBar);
        int tintColor = a.getInt(R.styleable.TintHorizProgressBar_tint, 0x0);
        a.recycle();

        setTint(tintColor);
    }

    @Override
    public void setLayoutParams(ViewGroup.LayoutParams params)
    {
        if (params instanceof ViewGroup.MarginLayoutParams)
        {
            ((ViewGroup.MarginLayoutParams) params).topMargin =
            ((ViewGroup.MarginLayoutParams) params).bottomMargin =
                    // Floor so it looks right on xxhdpi as well as ldpi
                    (int) Math.floor(-6.2f * getResources().getDisplayMetrics().density);
        }
        super.setLayoutParams(params);
    }

    public void setTint(int color)
    {
        getIndeterminateDrawable().setColorFilter(color, PorterDuff.Mode.SRC_IN);
    }
}
