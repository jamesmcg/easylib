/* No License */

package com.jamesmcg.easylib.ui;

import com.jamesmcg.easylib.R;
import com.jamesmcg.easylib.misc.TypefaceCache;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.EditText;

public class FontEditText extends EditText
{
    public FontEditText(Context context, AttributeSet attrs)
    {
        super(context, attrs);
        construct(context, attrs);
    }

    public FontEditText(Context context, AttributeSet attrs, int defStyle)
    {
        super(context, attrs, defStyle);
        construct(context, attrs);
    }

    private void construct(Context context, AttributeSet attrs)
    {
        if (isInEditMode())
        {
            // App singleton doesn't work in the layout editor, make it look "unusual" so we know it will change
            setTypeface(Typeface.create("monospace", Typeface.NORMAL));
            return;
        }

        TypedArray values = context.obtainStyledAttributes(attrs, R.styleable.FontEditText);
        String fontPath = values.getString(R.styleable.FontEditText_fontPath);
        values.recycle();

        if (fontPath == null) throw new IllegalArgumentException("Please specify a fontPath");

        Typeface typeface = TypefaceCache.getTypeface(context, fontPath);

        if (typeface == null) throw new IllegalArgumentException("Nonexistant typeface path (" + fontPath + ")");
        else setTypeface(typeface);
    }
}
