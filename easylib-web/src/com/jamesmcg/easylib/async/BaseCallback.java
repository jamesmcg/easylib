/*
Copyright 2014 James E McGuire

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

BaseCallback
*/

package com.jamesmcg.easylib.async;

public interface BaseCallback
{
	/**
     *    [For a task inside a TaskContainer]
     * 1. Called from TaskContainer.addAndCall() if the task is already started and is still in progress
     * 2. Called from TaskContainer.startTask() if the callback is already added and listening
     *
     *    [A raw task, no container]
     * 1. Called from BaseTask.addAndCallNow() only if the task has been executed and is still in progress
     * 2. Called from BaseTask.executeSerial() or BaseTask.executeParallel() if the callback is already added and listening
     */
	public void started();

	/**
     *    [For a task inside a TaskContainer]
     * 1. Called from TaskContainer.addAndCall() if no task is running (inc. previously completed with TASK_RESULT_NORMAL, inc. previously cancelled - isCancelled will be FALSE)
     * 2. Called from TaskContainer.addAndCall() if the task has already completed (only with TaskContainer.Type.TASK_RESULT_IMPORTANT, after any other "onPostExecute" callbacks)
     * 3. Called from TaskContainer.cancel()
     *
     *    [A raw task, no container]
     * 1. Called from BaseTask.addAndCallNow() if the task was previously cancelled
     * 2. Called from BaseTask.addAndCallNow() if the task has already completed (after any other "onPostExecute" callbacks)
     * 3. Called from BaseTask.cancel()
	 */
	public void stopped(boolean isCancelled);
}
