/*
Copyright 2014 James E McGuire

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

SimpleCallback
*/

package com.jamesmcg.easylib.async;

/**
 * Created by James on 27/08/2014.
 */
public interface SimpleCallback<R extends WebResponse> extends BaseCallback
{
    /** This method will always be followed by a call to SimpleCallback.stopped() */
    public void success(WebSuccessData<R> result);

    /** This method will always be followed by a call to SimpleCallback.stopped() */
    public void failure(WebFailedData result);
}
