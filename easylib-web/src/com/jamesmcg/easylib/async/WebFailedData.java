/*
Copyright 2014 James E McGuire

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

WebFailedData
*/

package com.jamesmcg.easylib.async;

import com.jamesmcg.easylib.web.EasyHttp;
import com.jamesmcg.easylib.async.WebTask.FailType;

/**
 * Created by James on 26/08/2014.
 */
public class WebFailedData
{
    private final String msg;
    public final FailType failType;
    /**
     * This is deliberately kept as Object because the result data should not normally be accessed.
     * Cast if you really need it
     * The result data will be null for FailType.CONNECTION
     * The result data will have failed validation for FailType.VALIDATE_MSG or FailType.SERVER_MSG
     */
    public final EasyHttp.Result<? extends WebResponse> httpResult;
    public final WebTask<?, ?> task;

    // Package-private for WebTask
    public WebFailedData(String msg, FailType failType, EasyHttp.Result<? extends WebResponse> httpResult, WebTask<?, ?> task)
    {
        this.msg = msg;
        this.failType = failType;
        this.httpResult = httpResult;
        this.task = task;
    }

    public String getMsgTitle()
    {
        return failType != null ? failType.description : "Error";
    }

    public String getMsg()
    {
        return msg != null ? msg : "No message";
    }

    public String getMsgFormatted()
    {
        return getMsgTitle() + ": " + getMsg();
    }
}
