/*
Copyright 2014 James E McGuire

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

WebResponse
*/

package com.jamesmcg.easylib.async;

import android.util.Log;

import java.util.Collection;
import java.util.Iterator;

public abstract class WebResponse
{
	public final static String VALID = ":-) OK";

    private enum Flag
    {
        FORBID_NULL,
        ALLOW_NULL
    }

    public enum ChildFlag
    {
        ALLOW_NULL,
        FORBID_NULL,
    }

    protected static boolean isInvalid(String validateOK)
    {
        if (validateOK == null) throw new IllegalArgumentException("validateOK() should never return null");
        return ! validateOK.equals(VALID);
    }

    protected static void removeInvalidNullSafe(Collection<? extends WebResponse> list, ChildFlag childFieldFlag)
    {
        if (childFieldFlag == null) throw new IllegalArgumentException("childFieldFlag == null");
        if (list == null) return;

        Iterator<? extends WebResponse> iterator = list.iterator();
        for (int i = 0; iterator.hasNext(); i++)
        {
            WebResponse r = iterator.next();
            if (r == null)
            {
                if (childFieldFlag == ChildFlag.FORBID_NULL)
                {
                    Log.i("EasyLib:WebResponse", "removeInvalid removed null: list[" + i + "]");
                    iterator.remove(); // Keep incrementing i after this
                }
            }
            else
            {
                String validateOKMsg = r.validateOK();
                if (isInvalid(validateOKMsg))
                {
                    Log.i("EasyLib:WebResponse", "removeInvalid removed list[" + i + "]: " + validateOKMsg);
                    iterator.remove(); // Keep incrementing i after this
                }
            }
        }
    }

    protected static String validateOKForbidNull(WebResponse object)
    {
        return validateOKNullSafe(object, Flag.FORBID_NULL);
    }

    protected static String validateOKForbidNull(WebResponse[] array, ChildFlag childFlag)
    {
        return validateOKNullSafe(array, Flag.FORBID_NULL, childFlag);
    }

    protected static String validateOKForbidNull(Collection<? extends WebResponse> list, ChildFlag childFlag)
    {
        return validateOKNullSafe(list, Flag.FORBID_NULL, childFlag);
    }

    protected static String validateOKAllowNull(WebResponse object)
    {
        return validateOKNullSafe(object, Flag.ALLOW_NULL);
    }

    protected static String validateOKAllowNull(WebResponse[] array, ChildFlag childFlag)
    {
        return validateOKNullSafe(array, Flag.ALLOW_NULL, childFlag);
    }

    protected static String validateOKAllowNull(Collection<? extends WebResponse> list, ChildFlag childFlag)
    {
        return validateOKNullSafe(list, Flag.ALLOW_NULL, childFlag);
    }

    private static String validateOKNullSafe(WebResponse object, Flag flag)
    {
        if (object == null) return flag == Flag.FORBID_NULL ? "object is null" : VALID;
        else return object.validateOK();
    }

    private static String validateOKNullSafe(WebResponse[] array, Flag flag, ChildFlag childFlag)
    {
        if (flag == null) throw new IllegalArgumentException("nullFlag == null");
        if (childFlag == null) throw new IllegalArgumentException("childFieldFlag == null");

        if (array == null) return flag == Flag.FORBID_NULL ? "array is null" : VALID;

        for (int i = 0; i < array.length; i++)
        {
            if (array[i] == null)
            {
                if (childFlag == ChildFlag.FORBID_NULL) return "array[" + i + "] is null";
            }
            else
            {
                String validateR = array[i].validateOK();
                if (isInvalid(validateR)) return validateR;
            }
        }

        return VALID;
    }

    private static String validateOKNullSafe(Collection<? extends WebResponse> list, Flag flag, ChildFlag childFlag)
    {
        if (flag == null) throw new IllegalArgumentException("nullFlag == null");
        if (childFlag == null) throw new IllegalArgumentException("childFieldFlag == null");

        if (list == null) return flag == Flag.FORBID_NULL ? "list is null" : VALID;

        Iterator<? extends WebResponse> iterator = list.iterator();
        for (int i = 0; iterator.hasNext(); i++)
        {
            WebResponse r = iterator.next();
            if (r == null)
            {
                if (childFlag == ChildFlag.FORBID_NULL) return "list[" + i + "] is null";
            }
            else
            {
                String validateR = r.validateOK();
                if (isInvalid(validateR)) return validateR;
            }
        }

        return VALID;
    }

	/**
	 * Called before validateOK(), return any string != VALID to report failure and prevent validateOK() being called
	 * Return VALID only if there isn't an error message from the remote server to be given to the failed callback
	 */
	protected String checkServerOK()
	{
		return VALID;
	}
	
	/**
	 * Called after checkServerOK(), if that returned VALID
	 * Check all response parameters for null and perform any initialisation as necessary
	 * Return VALID if everything is ok, or an error identifying the local issue if not
	 */
	protected abstract String validateOK();
}
