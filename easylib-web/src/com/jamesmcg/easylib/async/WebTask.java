/*
Copyright 2014 James E McGuire

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

WebTask
*/

package com.jamesmcg.easylib.async;

import java.util.List;

import com.jamesmcg.easylib.web.EasyHttp.Result;

public abstract class WebTask<R extends WebResponse, C extends BaseCallback> extends BaseTask<C, Void, Result<R>>
{
	public enum FailType 
	{
		CONNECTION ("Unable to connect"),
		VALIDATE_MSG ("Bad response"),
		SERVER_MSG ("Server error");

        public final String description;

        private FailType(String description) {
            this.description = description;
        }
	}
	
	// These methods cannot be implemented here because this class doesn't know what methods C has,
	// And in fact the superclass may want to give C more than just failure and success methods.
	// CheckedCallbackTask is normally appropriate (and has just failure/success)
	// but if you need more complex callbacks then override this class instead
	protected abstract void onExecuteCallbacksFailed(List<C> callbacks, WebFailedData result);
	protected abstract void onExecuteCallbacksSuccess(List<C> callbacks, WebSuccessData<R> result);
	
	protected void preCallbacks(List<C> callbacks) { }
	protected void postCallbacks(List<C> callbacks) { }
	
	/** This method is final so it can't be overridden. Add functionality as listeners here instead of overriding again (which gets confusing) */
	@Override
	protected final void onExecuteCallbacks(List<C> callbacks, Result<R> result)
	{
		preCallbacks(callbacks);

		String errorMsg;
		if ( ! result.success)
		{
            onExecuteCallbacksFailed(callbacks, new WebFailedData(result.errorData.readableMsg, FailType.CONNECTION, result, this));
		}
		else if ( ! WebResponse.VALID.equals(errorMsg = result.data.checkServerOK()))
		{
			onExecuteCallbacksFailed(callbacks, new WebFailedData(errorMsg, FailType.SERVER_MSG, result, this));
		}
		else if ( ! WebResponse.VALID.equals(errorMsg = result.data.validateOK()))
		{
			onExecuteCallbacksFailed(callbacks, new WebFailedData(errorMsg, FailType.VALIDATE_MSG, result, this));
		}
		else
		{
			onExecuteCallbacksSuccess(callbacks, new WebSuccessData<R>(result, this));
		}

        // postCallbacks will be called by onExecuteStoppedCallbacks, which is called by BaseTask
        // (with the same list of callbacks)
	}
	
	@Override
	protected final void onExecuteStartedCallbacks(List<C> callbacks)
	{
		preCallbacks(callbacks);
		super.onExecuteStartedCallbacks(callbacks);
		postCallbacks(callbacks);
	}

    @Override
	protected final void onExecuteStoppedCallbacks(List<C> callbacks, boolean isCancelled)
	{
        // if onExecuteCallbacks was called there is no need to call preCallbacks
        if (!isDone()) preCallbacks(callbacks);

		super.onExecuteStoppedCallbacks(callbacks, isCancelled);
		postCallbacks(callbacks);
	}

    /**
     * Optional override if you need to pass any data through the task,
     * useful in case you use an anonymous declaration of your WebTask
     */
    public Object getExtra() {
        return null;
    }
}
