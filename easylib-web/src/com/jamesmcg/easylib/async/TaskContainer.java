/*
Copyright 2014 James E McGuire

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

TaskContainer
*/

package com.jamesmcg.easylib.async;

import java.util.LinkedHashMap;
import com.jamesmcg.easylib.async.BaseTask.OnPreExecuteCallbacksListener;
import android.annotation.TargetApi;
import android.os.Build;
import android.os.Handler;
import android.util.Log;

/**
 * Point of this class:
 * 1. to allow "Callbacks" to stay added, and they will be readded and all called as new tasks are started from the container
 * 2. to prevent potential race conditions in the callback method order (i.e. starting() for the new task followed complete()
 *    for the old task) if starting a new task from within one of the callbacks that is being called
 * 3. TASK_RESULT_IMPORTANT should be used to prevent a task being forgotten about if it did something important like
 *    "registration", for example after this a registration fragment should disappear if possible, or disappear in onStart
 *    (at the next earliest opportunity)  
 * 
 * When using these web tasks, to use them properly there are 3 things that need doing:
 * 1. onStart register
 * 2. onStart update ui to match task (if running or not, use TaskRunListener)
 * 3. onStop unregister
 */
public class TaskContainer<C extends BaseCallback>
{
	public enum Type
	{
		/**
		 * Only use this if the task is accessed in one place (i.e. registration fragment). Result is kept until cancel() is called.
		 * Use only for web tasks that will affect one of these:
		 * 1. a database somewhere on the remote server (i.e. submit some data), and it should only be called once
		 * 2. local copy of data from the server which is affected by the web call, and the app cache needs to update on resume
		 */
		TASK_RESULT_IMPORTANT,
		/**
		 * Task is set to null as soon as it completes (before the callbacks are fired off).
		 * Callbacks not attached when the web call completes won't get the success or failure call.
		 */
		TASK_RESULT_NORMAL
	}
	
	// Support version
	private static BaseCallback callbackShowOrHideModalDialog(
			final Object fragmentActivityOrFragment,
			final android.support.v4.app.DialogFragment dialog,
			final String fragmentTag)
	{
		return new BaseCallback()
		{
			private android.support.v4.app.FragmentManager getSupportFragmentManager()
			{
				if (fragmentActivityOrFragment instanceof android.support.v4.app.FragmentActivity) return ((android.support.v4.app.FragmentActivity) fragmentActivityOrFragment).getSupportFragmentManager();
				else if (fragmentActivityOrFragment instanceof android.support.v4.app.Fragment) return ((android.support.v4.app.Fragment) fragmentActivityOrFragment).getActivity().getSupportFragmentManager();
				else throw new RuntimeException("Never happen case");
			}
			
			@Override
			public void stopped(boolean isCancelled)
			{
				android.support.v4.app.Fragment d = getSupportFragmentManager().findFragmentByTag(fragmentTag);
				if (d != null)
				{
					if (d == dialog) dialog.dismiss();
					else if (d instanceof android.support.v4.app.DialogFragment)
					{
						String originalArgs = "" + dialog.getArguments();
						String dismissingArgs = "" + d.getArguments();
						
						Log.i("CallbackTaskContainer.TaskRunListener",
								"Stopping: findFragmentByTag dialog " + d
								+ " different to original dialog " + dialog
								+ (originalArgs.equals(dismissingArgs) ? "" : (" originalArgs " + originalArgs + " dismissingArgs " + dismissingArgs)));
						((android.support.v4.app.DialogFragment) d).dismiss();
					}
					else Log.e("CallbackTaskContainer.TaskRunListener", "Incorrect fragment tags: findFragmentByTag dialog " + d + " is not a android.support.v4.app.DialogFragment");
				}
			}
			
			@Override
			public void started()
			{
				android.support.v4.app.Fragment d = getSupportFragmentManager().findFragmentByTag(fragmentTag);
				if (d == null)
				{
					dialog.show(getSupportFragmentManager(), fragmentTag);
				}
			}
		};
	}
	
	// Non-support version
	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	private static BaseCallback callbackShowOrHideModalDialog(
			final Object activityOrFragment,
			final android.app.DialogFragment dialog,
			final String fragmentTag)
	{
		return new BaseCallback()
		{
			private android.app.FragmentManager getFragmentManager()
			{
				if (activityOrFragment instanceof android.app.Activity) return ((android.app.Activity) activityOrFragment).getFragmentManager();
				else if (activityOrFragment instanceof android.app.Fragment) return ((android.app.Fragment) activityOrFragment).getActivity().getFragmentManager();
				else throw new RuntimeException("Never happen case");
			}
			
			@Override
			public void stopped(boolean isCancelled)
			{
				android.app.Fragment d = getFragmentManager().findFragmentByTag(fragmentTag);
				if (d != null)
				{
					if (d == dialog) dialog.dismiss();
					else if (d instanceof android.app.DialogFragment)
					{
						String originalArgs = "" + dialog.getArguments();
						String dismissingArgs = "" + d.getArguments();
						
						Log.i("CallbackTaskContainer.TaskRunListener",
								"Stopping: findFragmentByTag dialog " + d
								+ " different to original dialog " + dialog
								+ (originalArgs.equals(dismissingArgs) ? "" : (" originalArgs " + originalArgs + " dismissingArgs " + dismissingArgs)));
						((android.app.DialogFragment) d).dismiss();
					}
					else Log.e("CallbackTaskContainer.TaskRunListener", "Incorrect fragment tags: findFragmentByTag dialog " + d + " is not a android.app.DialogFragment");
				}
			}
			
			@Override
			public void started()
			{
				android.app.Fragment d = getFragmentManager().findFragmentByTag(fragmentTag);
				if (d == null)
				{
					dialog.show(getFragmentManager(), fragmentTag);
				}
			}
		};
	}

	public static BaseCallback callbackShowOrHideModalDialog(
			final android.support.v4.app.FragmentActivity activity,
			final android.support.v4.app.DialogFragment dialog,
			final String fragmentTag)
	{
		return callbackShowOrHideModalDialog((Object) activity, dialog, fragmentTag);
	}
	
	public static BaseCallback callbackShowOrHideModalDialog(
			final android.support.v4.app.Fragment fragment,
			final android.support.v4.app.DialogFragment dialog,
			final String fragmentTag)
	{
		return callbackShowOrHideModalDialog((Object) fragment, dialog, fragmentTag);
	}
	
	public static BaseCallback callbackShowOrHideModalDialog(
			final android.app.Activity activity,
			final android.app.DialogFragment dialog,
			final String fragmentTag)
	{
		return callbackShowOrHideModalDialog((Object) activity, dialog, fragmentTag);
	}
	
	public static BaseCallback callbackShowOrHideModalDialog(
			final android.app.Fragment fragment,
			final android.app.DialogFragment dialog,
			final String fragmentTag)
	{
		return callbackShowOrHideModalDialog((Object) fragment, dialog, fragmentTag);
	}

	private Type type;
	private BaseTask<C, ?, ?> internalTask = null;
	
	/*
	 * We need to use some tag to prevent duplicate callbacks for the rare case when a fragment is destroyed
	 * early and onStop isn't called. When its restored onStart would otherwise add a second callback.
	 * WeakReference does not work because I have seen Android not garbage collect these fragments, and it would
	 * also be unpredictable. As a bonus the tag allows anonymous callbacks to be added and removed later
	 */
	private LinkedHashMap<String, C> autoAddCallbacks = null;

	/*
	 * whoHoldsThisContainer is used simply to check you're using this right :)
	 * Don't create an instance of a container within a Fragment, because its pointless (use application or activity),
	 * Use a stand-alone CheckedCallbackTask for a "throwaway" fragment task...
	 */
	public TaskContainer(Type type)
	{
		/*if (whoHoldsThisContainer instanceof android.app.Application == false && whoHoldsThisContainer instanceof android.app.Activity == false)
		{
			throw new RuntimeException("Declare a TaskContainer ONLY within activities or application singleton."
					+ " Not static, within Fragments or within DialogFragments (because of their shorter lifecycle)");
		}*/
		
		this.type = type;
	}

	// <X extends C> is necessary for the java 7 <> notation to pick up the correct generic types
	/** Convenience constructor which also adds a callback, using addAndCall */
	public <X extends C> TaskContainer(Type type, X callbackToAdd)
	{
		this(type);
		
		// Stopped callback will always be fired from here (internalTask == null)
		_addAndCall(null, callbackToAdd);
	}
	

	/*public static <R, X extends BaseCallbackTask<C, ?, ?> & TaskWithSuccessI<R>, C> R getSuccessResponseOrNull(TaskContainer<X, C> container)
	{
		if (container.isTaskDone() && container.internalTask.isSuccess())
		{
			return container.internalTask.getSuccessData();
		}
		else return null;
	}
	
	public boolean isTaskDone()
	{
		if (type != Type.KEEP_RESULT_FOREVER) throw new RuntimeException("This method only makes sense when type == KEEP_RESULT_FOREVER");
		return internalTask != null && internalTask.isDone();
	}*/
	
	/**
	 * Returns true if there is no task / it is stopped 
	 * Returns false if using KEEP_RESULT_FOREVER and the task has completed
	 */
	public boolean isTaskStopped()
	{
		return internalTask == null;
	}
	
	// This should be null if internalTask.isDone() because internalTask should not be read during callbacks (as it can change if a new task is started)
	// Even though Type.TASK_RESULT_DELETED initially keeps the task, it can be set to something else by a call to startTask.
	// Because of this the "Completed Task" needs to be remembered a different way, ideally by ValidatedCallbackTask
	/** This is null from within any one of the callbacks (with the exception of started) */
	public BaseTask<C, ?, ?> getOngoingTask()
	{
		if (internalTask == null || internalTask.isDone()) return null;
		else return internalTask;
	}
	
	/**
	 * Cancels a task if running, or clears it if using KEEP_RESULT_FOREVER and the task has completed.
	 * This can be called from one of the Task's callbacks if it wishes to start another task.
	 * If cancel() is called from within one of the Task's callbacks, there WON'T be another callback for cancel.
	 */
	public void cancel()
	{
		if (isTaskStopped() == false)
		{
			// If the task is Done, internalTask.cancel() won't do anything
			internalTask.cancel();
			internalTask = null;
		}
	}
	
	/** Starts the task. Calls any TaskRunListeners (if present) from here. */
	public <T extends BaseTask<C, ?, ?>> void startTask(T task)
	{
		if (isTaskStopped() == false) throw new IllegalArgumentException("Attempted to start already running (or saved) task, either deal with the case when its running or call cancel()");

		Log.i("TaskContainer", task + " starting");
		
		internalTask = task;
		
		if (autoAddCallbacks != null)
		{
			// Don't fire "stopped" callbacks as "started" is about to follow immediately (on a new handler)
			for (C c : autoAddCallbacks.values()) internalTask.addAndCallNow(c, false);
		}

		// Allow a task to be started from within one of the completed task callbacks, even if more callbacks still remain,
		// otherwise all callbacks will receive started(), followed (incorrectly) by those remaining callbacks of success()
		// or failed() or cancelled()
        final T runnableTask = task;
        Log.e("TaskContainer", "pending");
		new Handler().post(new Runnable()
		{
			@Override
			public void run()
			{
                Log.e("TaskContainer", "here");

                // Deal with null task where cancel() was called before Runnable starts
                // Deal with a different task where cancel() and startTask() were both called quickly, before Runnable starts (multiple runnables pending)
                if (internalTask != runnableTask) return;

				// Throw this crash before it happens for real, so we can add offending class name to the message
				if (internalTask.isStatusPending() == false)
				{
					throw new IllegalArgumentException("Cannot execute task: the task is already running (" + internalTask.getClass().getName() + ")");
				}

				// This throws an exception if the task has already been executed
				// (especially without our OnPreExecuteCallbacksListener object)
				internalTask.executeParallel(new OnPreExecuteCallbacksListener()
				{
					@Override
					public void onPreExecuteCallbacks()
					{
						if (type == Type.TASK_RESULT_NORMAL) internalTask = null;
					}
		
					@Override
					public void onCancelled()
					{
						internalTask = null;
					}
				});
			}
		});
	}

	/**
	 * This should be called in onStart with any listeners you use
	 * 
	 * Will add this callback to any future calls of startTask()
	 * Will call this callback immediately with the appropriate method
	 */
	public void addAndCall(String cKey, C c)
	{
		if (cKey == null) throw new IllegalArgumentException("cKey == null");
		_addAndCall(cKey, c);
	}
	
	private void _addAndCall(String cKeyOrNull, C c)
	{
		//if (type != Type.KEEP_RESULT_FOREVER) throw new IllegalArgumentException("add(Callback) only applies to Type.KEEP_RESULT_FOREVER");
		if (c == null) throw new IllegalArgumentException("c == null");
		
		if (autoAddCallbacks == null) autoAddCallbacks = new LinkedHashMap<String, C>();
		
		C replacedOrNull = autoAddCallbacks.put(cKeyOrNull, c);
		if (replacedOrNull != null && isTaskStopped() == false) internalTask.remove(replacedOrNull);
		
		if (isTaskStopped()) c.stopped(false);
		else internalTask.addAndCallNow(c);
	}
	
	/** Convenience method, This is not a method which should be used normally, but  */
	public void call(C c)
	{
		if (c == null) throw new IllegalArgumentException("c == null");

		// (internalTask is nulled as soon as its cancelled)
		if (isTaskStopped()) c.stopped(false);
		else if (internalTask.isDone()) internalTask.addAndCallNow(c); // THIS WONT ADD C TO THE TASK, it clears instantly because the task is complete
		else c.started(); // do this even if internalTask.isRunning() == false, because it will always start asap; The only way to set internalTask is through startTask(T)
	}
	
	/** This should be called in onStop with any listeners you added in onStart */
	public void remove(String cKey)
	{
		if (cKey == null) throw new IllegalArgumentException("cKey == null");
		
		if (autoAddCallbacks != null)
		{
			C removedOrNull = autoAddCallbacks.remove(cKey);

			if (removedOrNull != null && isTaskStopped() == false) internalTask.remove(removedOrNull);

			if (autoAddCallbacks.size() == 0) autoAddCallbacks = null;
		}
	}
}
