/*
Copyright 2014 James E McGuire

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

WebSuccessData
*/

package com.jamesmcg.easylib.async;

import com.jamesmcg.easylib.web.EasyHttp;

/**
 * Created by James on 26/08/2014.
 */
public class WebSuccessData<R>
{
    public final R data;
    public final EasyHttp.Result<R> httpResult;
    public final WebTask<?, ?> task;

    // Package-private for WebTask
    WebSuccessData(EasyHttp.Result<R> httpResult, WebTask<?, ?> task)
    {
        this.data = httpResult.data;
        this.httpResult = httpResult;
        this.task = task;
    }
}
