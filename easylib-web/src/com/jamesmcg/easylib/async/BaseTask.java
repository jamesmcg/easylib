/*
Copyright 2014 James E McGuire

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

BaseTask
*/

package com.jamesmcg.easylib.async;

import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.os.AsyncTask;
import android.os.AsyncTask.Status;
import android.os.Build;

public abstract class BaseTask<CallbackType extends BaseCallback, ProgressType, ResultType>
{
	/** package-private */
	interface OnPreExecuteCallbacksListener
	{
		/**
		 * This method will only ever be called once per task.
		 * Container should use handler to solve: if one callback calls cancel and then starttask, potentially
		 *     there are remaining callbacks to have their completed method called, but startTask will be
		 *     called on all the methods first, then those remaining callbacks will be fired
		 * Pre-callbacks means that internalTask will be null on the TaskContainer as any callbacks are fired,
		 *     1. the only downside is its a bit more complex to implement an accessible 'completed task' variable
		 * Post-callbacks means that internalTask will remain set on TaskContainer during callbacks
		 *     1. downside is if a callback wants to start a task again it has to call cancel() first (which is not a problem, just annoying)
		 *     2. if one task calls cancel internalTask will become null on the Container which might be an issue
		 *     for one of the other callbacks
		 */
		void onPreExecuteCallbacks();
		/** Called the first time cancel() is called on this task */
		void onCancelled();
	}
	
	private static final String fireStartedReentrantError = "Use a handler. Callbacks are forbidden from this method while within a started() callback";

	private final AsyncTask<Void, ProgressType, ResultType> actualTask = new AsyncTask<Void, ProgressType, ResultType>()
	{
		@Override
		protected ResultType doInBackground(Void... v)
		{
			return BaseTask.this.doInBackground();
		}
		
		@Override
		protected void onPostExecute(ResultType result)
		{
			BaseTask.this.onPrivatePostExecute(result);
		}
	};

	/** Override this method to call the appropriate Callback method, firing off the same one for every callback. */
	protected abstract void onExecuteCallbacks(List<CallbackType> callbacks, ResultType result);
	
	/** Override this method to do the actual work on a background thread */
	protected abstract ResultType doInBackground();
	
	// callbacks should be cloned (not cleared) to prevent superclass messing with elements (we also prevent anyone from
	// calling any offending methods from within that started callback (see firingStartedCallbacks variable)
	/** This method can be overridden to perform additional logic. Make sure to call super or perform the actual calls to started() yourself */
	protected void onExecuteStartedCallbacks(List<CallbackType> callbacks)
	{
		for (CallbackType c : callbacks) c.started();
	}
	
	// callbacks should have been cloned and cleared (with exception of singleStopped)
	/** This method can be overridden to perform additional logic. Make sure to call super or perform the actual calls to stopped(bool) yourself */
	protected void onExecuteStoppedCallbacks(List<CallbackType> callbacks, boolean isCancelled)
	{
		for (CallbackType c : callbacks) c.stopped(isCancelled);
	}
	
	private void singleStopped(CallbackType c, boolean isCancelled)
	{
		LinkedList<CallbackType> list = new LinkedList<CallbackType>();
		list.add(c);
		onExecuteStoppedCallbacks(list, isCancelled);
	}
	
	private void singleStarted(CallbackType c)
	{
		LinkedList<CallbackType> list = new LinkedList<CallbackType>();
		list.add(c);
		onExecuteStartedCallbacks(list);
	}
	
	/** package-private, priorityCallback must not call cancel() on this task */
	private OnPreExecuteCallbacksListener onPreExecuteCallbackListener = null;
	
	private final LinkedHashSet<CallbackType> callbacks = new LinkedHashSet<CallbackType>();
	private ResultType result = null;
	private boolean firingStartedCallbacks; // A sort of Re-entrant lock because started() is the only method where callbacks are not cleared before firing... (and multiple callbacks are fired)

	// Copy to prevent ConcurrentModificationExceptions and some other container situation I can't remember
	private List<CallbackType> cloneCallbacksAndClear()
	{
		List<CallbackType> beforeClear = new LinkedList<CallbackType>(callbacks);
		
		callbacks.clear();
		
		return beforeClear;
	}
	
	private void onPrivatePostExecute(ResultType result)
	{
        if (result == null) throw new RuntimeException("doInBackground method cannot return null");

		this.result = result;
		
		List<CallbackType> clearedCallbacks = cloneCallbacksAndClear();

		OnPreExecuteCallbacksListener p = onPreExecuteCallbackListener;
		onPreExecuteCallbackListener = null;
		if (p != null) p.onPreExecuteCallbacks();
		
		onExecuteCallbacks(clearedCallbacks, result);
        onExecuteStoppedCallbacks(clearedCallbacks, false);
	}
	
	protected ResultType getResult()
	{
		return result;
	}
	
	/** Add the callback to this task and let the task call it right now, including the result if the task has already finished doInBackground */
	public void addAndCallNow(CallbackType c)
	{
		addAndCallNow(c, true);
	}
	
	/** package-private because its also used by TaskContainer */
	void addAndCallNow(CallbackType c, boolean fireNotStartedYetCallback)
	{
		if (firingStartedCallbacks) throw new RuntimeException(fireStartedReentrantError);
		
		if (c == null) throw new IllegalArgumentException("can't add a null callback");
		
		if (actualTask.isCancelled())
		{
			// callbacks must remain empty at this point as the task has finished for good
			singleStopped(c, true);
		}
		else if (actualTask.getStatus() == Status.PENDING)
		{
			// Task hasn't started yet
			callbacks.add(c);
			
			// This is so TaskContainer can prevent this callback when it knows the started callback is going to be fired immediately after
			if (fireNotStartedYetCallback) singleStopped(c, false);
		}
		else if (result == null)
		{
			// Task is in progress
			callbacks.add(c);
			singleStarted(c);
		}
		else // result != null
		{
			// callbacks must remain empty at this point as the task has finished for good
			LinkedList<CallbackType> singleCallback = new LinkedList<CallbackType>();
			singleCallback.add(c);
			
			onExecuteCallbacks(singleCallback, result);
            onExecuteStoppedCallbacks(singleCallback, false);
		}
	}
	
	/** Removes the callback (if present) */
	public void remove(CallbackType callback)
	{
		if (firingStartedCallbacks) throw new RuntimeException(fireStartedReentrantError);
		
		callbacks.remove(callback);
	}
	
	/** Cancel this task if not completed */
	public void cancel()
	{
		if (firingStartedCallbacks) throw new RuntimeException(fireStartedReentrantError);
		
		if (isDone())
		{
			// What ya calling cancel for? callbacks.size() is always 0 after the task is done
		}
		else if (actualTask.isCancelled() == false)
		{
			actualTask.cancel(false);
			
			List<CallbackType> clearedCallbacks = cloneCallbacksAndClear();
			
			OnPreExecuteCallbacksListener p = onPreExecuteCallbackListener;
			onPreExecuteCallbackListener = null;
			if (p != null) p.onCancelled();
			
			onExecuteStoppedCallbacks(clearedCallbacks, true);
		}
	}
	
	public boolean isCancelled()
	{
		return actualTask.isCancelled();
	}
	
	public boolean isRunning()
	{
		return ! isDone() && ! isCancelled() && actualTask.getStatus() == Status.PENDING;
	}
	
	/** package-private for use with TaskContainer */
	boolean isStatusPending()
	{
		return actualTask.getStatus() == Status.PENDING;
	}
	
	public boolean isDone()
	{
		return result != null;
	}
	
	/** package-private for use with TaskContainer */
	void executeParallel(OnPreExecuteCallbacksListener listener)
	{
		onPreExecuteCallbackListener = listener;
		executeParallel();
	}
	
	/**
	 * Prefer executeParallel() if possible.
	 * This serial method is only available for API 11 or higher anyway.
	 * Executes serially in order of start time, waiting for any other running Android AsyncTask
	 */
	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	public void executeSerialAPI11()
	{
		actualTask.executeOnExecutor(AsyncTask.SERIAL_EXECUTOR, (Void[]) null);
		
		firingStartedCallbacks = true;
		onExecuteStartedCallbacks(new LinkedList<CallbackType>(callbacks)); // Clone list to prevent superclass messing with elements (also via firingStartedCallbacks checks)
		firingStartedCallbacks = false;
	}
	
	/** Uses the main AsyncTask thread pool executor. */
	@SuppressLint({ "InlinedApi", "NewApi" })
	public void executeParallel()
	{
		if (Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB)
		{
			// Asynctask is already parallel...
			actualTask.execute();
		}
		else
		{
			actualTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, (Void[]) null);
		}
		
		firingStartedCallbacks = true;
		onExecuteStartedCallbacks(new LinkedList<CallbackType>(callbacks)); // Clone list to prevent superclass messing with elements (also via firingStartedCallbacks checks)
		firingStartedCallbacks = false;
	}
}
