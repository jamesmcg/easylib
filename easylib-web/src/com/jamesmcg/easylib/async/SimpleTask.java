/*
Copyright 2014 James E McGuire

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

SimpleTask
*/

package com.jamesmcg.easylib.async;

import java.util.List;

/**
 * If you need different callback parameters to success or failure:
 * 1. Copy this class
 * 2. Import ValidatedCallbackTask
 * 3. Change the Callback methods to add the parameters you need
 * 
 * To best use these web tasks:
 * 1. Create an instance of a TaskContainer for the right type of task in either an Activity or Application instance (not Fragment)
 * 2. Use addAndCall method on the container from ONSTART
 * 3. Use remove method on the container from ONSTOP
 * 4. Use the startTask method on the container when you want to start the task
 */
public abstract class SimpleTask<R extends WebResponse, C extends SimpleCallback<R>>
		extends WebTask<R, C>
{
    @Override
    protected final void onExecuteCallbacksFailed(List<C> callbacks, WebFailedData params)
    {
        for (C c : callbacks) c.failure(params);
    }

    @Override
    protected final void onExecuteCallbacksSuccess(List<C> callbacks, WebSuccessData<R> params)
    {
        for (C c : callbacks) c.success(params);
    }
}
