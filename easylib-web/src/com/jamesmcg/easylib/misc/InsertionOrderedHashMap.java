/*
Copyright 2012 James E McGuire

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

InsertionOrderedHashMap
*/

package com.jamesmcg.easylib.misc;

import java.util.HashMap;

/**
 *  This is basically a LinkedList but with the ability to check if an element exists
 *  in it, and move that element to the front of the queue in O(1) time.
 *  
 *  Due to the overheads, a LinkedList may be better if n is small.
 *  
 *  An implementation for an insertion ordered HashMap which keeps the fast O(1)
 *  HashMap accesses while allowing O(1) accesses to the oldest or most recent
 *  elements.
 *  
 *  LinkedHashMap from the Java standard library only supports O(n)
 *  time for access to the newest element, O(1) for the oldest. Obviously getting
 *  its iterator and calling it.next() n times to get the oldest is a stupid solution.
 *  
 *  This also differs in insertion order to LinkedHashMap. In this implementation a
 *  put() where a key is re-inserted will bring that key/value to the newest end of
 *  the ordered list.
 *  
 *  (A key k is reinserted into a map m if m.put(k, v) is invoked when m.containsKey(k)
 *  would return true immediately prior to the invocation.)
 *  
 *  Note unlike BidiMap, this class allows multiple keys to map to the same value, BUT
 *  in this situation newestKey will refer to the key used most recently, as will newestValue.
 *  (all of) the other keys used will remain in whatever order.
 *  
 *  Note that this class will keep hold of all the keys in use, to allow use of
 *  getNewestKey() and getOldestKey(). They will NOT be garbage collected.
 *  This is the same as what you would get with a HashMap. */
public class InsertionOrderedHashMap <K, V>
{
	/** Unfortunately we have to store the keys alongside in order to allow getting of keys
	 *  alongside data. E.g. when you remove the newest element to keep getNewestKey() up
	 *  to data, we have to somehow get the key for the second oldest element. That's not
	 *  possible in O(1) time unless you keep it alongside. Else you'd be iterating a list */
	private class OrderedItem <X, Y>
	{
		public OrderedItem(OrderedItem<X, Y> older, OrderedItem<X, Y> newer, X key, Y data)
		{
			this.older = older;
			this.newer = newer;
			this.key = key;
			this.data = data;
		}
		
		public OrderedItem<X, Y> older;
		public OrderedItem<X, Y> newer;
		public X key;
		public Y data;
	}

	private final HashMap<K, OrderedItem<K, V>> map;
	private OrderedItem<K, V> newestItem = null; // Use newestValue.newerValue = oldestValue to "wrap around" and store
	
	public InsertionOrderedHashMap()
	{
		map = new HashMap<K, OrderedItem<K, V>>();
	}
	
	public InsertionOrderedHashMap(int initialCapacity)
	{
		map = new HashMap<K, OrderedItem<K, V>>(initialCapacity);
	}
	
	public InsertionOrderedHashMap(int initialCapacity, float loadFactor)
	{
		map = new HashMap<K, OrderedItem<K, V>>(initialCapacity, loadFactor);
	}
	
	public K getNewestKey()
	{
		return newestItem == null ? null : newestItem.key;
	}
	
	public V getNewestValue()
	{
		return newestItem == null ? null : newestItem.data;
	}
	
	public K getOldestKey()
	{
		// no need to check if newestItem.newer is null. Just take care of size == 1 case
		return newestItem == null ? null : newestItem.newer.key;
	}
	
	public V getOldestValue()
	{
		// no need to check if newestItem.newer is null. Just take care of size == 1 case
		return newestItem == null ? null : newestItem.newer.data;
	}
	
	// If you want the key to match this value, just call getNewestKey() BEFORE popping it
	public V popNewestValue()
	{
		if (newestItem == null)
		{
			return null;
		}

		map.remove(newestItem.key);
		V retData = newestItem.data;
		
		// Merge the items either side of NewestItem
		// obviously merging itself to itself if there are actually no other elements
		newestItem.older.newer = newestItem.newer;
		newestItem.newer.older = newestItem.older;
		
		// if there was 1 element, before it was popped off, set newestItem to null
		newestItem = newestItem == newestItem.older ? null : newestItem.older;
		
		return retData;
	}

	// If you want the key to match this value, just call getOldestKey() BEFORE popping it
	public V popOldestValue()
	{
		if (newestItem == null)
		{
			return null;
		}

		map.remove(newestItem.newer.key);
		V retData = newestItem.newer.data;
		
		// Merge the items either side of NewestItem
		// obviously merging itself to itself if there are actually no other elements
		OrderedItem<K, V> oldestItem = newestItem.older;
		oldestItem.older.newer = oldestItem.newer;
		oldestItem.newer.older = oldestItem.older;
		
		if (map.size() == 0)
		{
			newestItem = null;
		}
		
		return retData;
	}
	
	public void clear()
	{
		map.clear();
		newestItem = null;
	}
	
	public boolean containsKey(K key)
	{
		return map.containsKey(key);
	}
	
	public boolean containsValue(V value)
	{
		return map.containsValue(value);
	}
	
	public V get(K key)
	{
		OrderedItem<K, V> oi = map.get(key);
		return oi == null ? null : oi.data;
	}
	
	public boolean isEmpty()
	{
		return map.isEmpty();
	}
	
	/** Associates the specified value with the specified key in this map.
	 *  If the map previously contained a mapping for this key, the old value is replaced.
	 *  @return previous value associated with specified key, or null if there was no mapping for key.
	 *  	A null return can also indicate that the HashMap previously associated null with the specified key. */
	public V put(K key, V value)
	{
		if (newestItem == null)
		{
			// Insert the first item in the set
			newestItem = new OrderedItem<K, V>(null, null, key, value);
			newestItem.newer = newestItem; // doubly linked list that wraps
			newestItem.older = newestItem; // doubly linked list that wraps
			
			map.put(key, newestItem);
			
			return null;
		}
		
		// push the old value for newestKey out, and populate the linkedlist
		// move the element linked to by key to the front
		
		OrderedItem<K, V> existing = map.get(key);
		V returnValue = existing == null ? null : existing.data;
		
		if (existing == null)
		{
			existing = new OrderedItem<K, V>(null, null, key, value); // older, newer, key, value
		}
		else
		{
			// item exists, replace it
			existing.data = value;
			
			// merge the two elements either side of the one we are replacing
			// so we can move the one we are replacing to the head of the linkedlist (newest)
			// essentially take the current element out of the list (except in the case of a single element)
			existing.older.newer = existing.newer;
			existing.newer.older = existing.older;
		}
		
		// place the existing element at the head of our list
		existing.older = newestItem;
		existing.newer = newestItem.newer;

		newestItem.newer.older = existing;
		newestItem.newer = existing;
		
		newestItem = existing;

		return returnValue;
	}
	
	/** Methods from HashMap I really CBA to implement */
	// public InsertionOrderedHashMap clone()
	
	// public Set entrySet()
	
	// public Set keySet()
	
	// public void putAll(Map m)
	
	// public Collection values()
	
	// Write an iterator
	
	// Overrides for equals and hashCode
}