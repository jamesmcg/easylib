/*
Copyright 2014 James E McGuire

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

TypefaceCache
*/

package com.jamesmcg.easylib.misc;

import java.lang.ref.WeakReference;
import java.util.HashMap;

import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Typeface;
import android.util.Log;

public class TypefaceCache
{
	private static HashMap<String, WeakReference<Typeface>> assetFileNameToTypeface = new HashMap<String, WeakReference<Typeface>>();
	
	private TypefaceCache() { }
	
	/**
	 * NOT thread safe, returns null if typeface doesn't exist or not enough memory, any other error etc.
	 * @param path - relative to the assets directory, e.g. "fonts/Roboto-Light.ttf"
	 */
	public static Typeface getTypeface(Context context, String path)
	{
		Typeface t;
		{
			WeakReference<Typeface> weakTypeface = assetFileNameToTypeface.get(path);
			t = weakTypeface == null ? null : weakTypeface.get();
		}
		
		if (t == null)
		{
			Log.e("com.jamesmcg.easylib.TypefaceCache", "Cache miss for " + path);
			
			AssetManager am = context.getAssets();
			
			try
			{
				t = Typeface.createFromAsset(am, path);
			}
			catch (Throwable uhoh)
			{
				return null;
			}
			
			assetFileNameToTypeface.put(path, new WeakReference<Typeface>(t));
		}
		
		return t;
	}
}
