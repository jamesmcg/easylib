/*
Copyright 2014 James E McGuire

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

EasyUtil
*/

package com.jamesmcg.easylib.misc;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;

import android.app.Activity;
import android.content.Context;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.IBinder;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

public class EasyUtil
{
    public static class Strings
    {
        public static boolean containsIgnoreCase(String str, String searchFor)
        {
            if (str == null || searchFor == null)
                return (str == null && searchFor == null); // null contains null
            else
                return str.toLowerCase().contains(searchFor.toLowerCase());
        }

        /**
         * Uses
         * 1. Decodes text with URLDecoder from url safe format (i.e. space = %20)
         * 2. No need for try/catch in your code
         * 3. Prevents less common runtime error crashes
         */
        public static String urlDecode(String text)
        {
            try
            {
                return URLDecoder.decode(text, "UTF-8");
            }
            catch (UnsupportedEncodingException e)
            {
                // Never happen case for Android
                e.printStackTrace();
                return text;
            }
            catch (java.lang.IllegalArgumentException e)
            {
                // e.g. "Invalid % sequence at 187: Terms &amp; Conditions for this campaign ..."
                e.printStackTrace();
                return text;
            }
        }

        /**
         * Uses
         * 1. Encodes text with URLEncoder to be url safe (i.e. space = %20)
         * 2. No need for try/catch in your code
         */
        public static String urlEncode(String text)
        {
            try
            {
                return URLEncoder.encode(text, "UTF-8");
            }
            catch (UnsupportedEncodingException e)
            {
                // Never happen case for Android
                e.printStackTrace();
                return text;
            }
        }
    }

    public static class Keyboards
    {
        /**
         * Uses
         * 1. Hide the keyboard properly, in a way that works consistently
         */
        public static void hide(Activity activity)
        {
            if (activity != null && activity.getWindow() != null)
            {
                hide(activity.getWindow().getDecorView().getRootView());
            }
        }

        /**
         * Uses
         * 1. Hide the keyboard properly, in a way that works consistently
         */
        public static void hide(View view)
        {
            IBinder windowToken = view == null ? null : view.getWindowToken();
            if (windowToken != null)
            {
                InputMethodManager imm = (InputMethodManager) view.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(windowToken, 0);
            }
        }

        /**
         * Uses
         * 1. Show the keyboard in a way compatible with hide(Activity)
         */
        public static void show(View view)
        {
            InputMethodManager imm = (InputMethodManager) view.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.showSoftInput(view, 0);
        }
    }

    public static class Views
    {
        @SuppressWarnings("deprecation")
        public static void setBackground(View view, Drawable drawable)
        {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN)
                view.setBackground(drawable);
            else
                view.setBackgroundDrawable(drawable);
        }
    }

    public static class PopupWindows
    {
        /**
         * Uses
         * 1. Dismiss the popup if the user clicks outside the popup
         */
        public static void dismissOnOutsideTouchIntercept(final android.widget.PopupWindow popup)
        {
            popup.setFocusable(false);
            popup.setOutsideTouchable(true);
            popup.setTouchInterceptor(new View.OnTouchListener()
            {
                @Override
                public boolean onTouch(View v, MotionEvent event)
                {
                    if (event.getAction() == MotionEvent.ACTION_OUTSIDE)
                    {
                        popup.dismiss();
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            });
        }

        /**
         * Uses
         * 1. To fix a bug where the PopupWindow normally scrolls content to make room for itself,
         *    but does not scroll enough if the anchor is taller than the screen (popup out of view)
         */
        public static void showAsDropDownAlwaysBelow(android.widget.PopupWindow popup, View anchor)
        {
            int[] mDrawingLocation = new int[2];
            int xoff = 0;
            int yoff = 0;
            anchor.getLocationInWindow(mDrawingLocation);
            int x = mDrawingLocation[0] + xoff;
            int y = mDrawingLocation[1] + anchor.getMeasuredHeight() + yoff;

            final Rect displayFrame = new Rect();
            anchor.getWindowVisibleDisplayFrame(displayFrame);

            final View root = anchor.getRootView();

            // THIS MATCHES THE INTERNAL LOGIC USED BY POPUPWINDOW, DO NOT CHANGE
            if (y + popup.getHeight() > displayFrame.bottom || x + popup.getWidth() - root.getWidth() > 0)
            {
                // if the drop down disappears at the bottom of the screen. we try to
                // scroll a parent scrollview or move the drop down back up on top of
                // the edit box
                int scrollX = anchor.getScrollX();
                int scrollY = anchor.getScrollY();
                Rect r = new Rect(scrollX, scrollY, scrollX + popup.getWidth(),
                        scrollY + popup.getHeight() + anchor.getMeasuredHeight());

                // requestRectangleOnScreen won't work properly in the case that the anchor is taller than the space available
                // (we want it to scroll right to the bottom of the anchor)
                //Log.e("MessengerAdapter", "r l" + r.left + " t" + r.top + " r" + r.right + " b" + r.bottom
                //        + " displayFrame l" + displayFrame.left + " t" + displayFrame.top + " r" + displayFrame.right + " b" + displayFrame.bottom);
                if (r.height() > displayFrame.height())
                {
                    r.offset(0, r.height());
                }

                // Calling requestRectangleOnScreen here (as PopupWindow normally would via showAsDropDown)
                // will mean PopupWindow's own logic (if(y + popup.getHeight() > displayFra...))
                // will resolve to false, and PopupWindow will accept the position we set here
                // (only if we force scroll to the bottom of anchor, if we force scroll to the top then PopupWindow won't like that)
                anchor.requestRectangleOnScreen(r, true);
            }

            popup.showAsDropDown(anchor);
        }
    }
}
