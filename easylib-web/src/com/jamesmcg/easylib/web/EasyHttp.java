/*
Copyright 2013 James E McGuire

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

EasyHttp
*/

package com.jamesmcg.easylib.web;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.net.URLEncoder;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.ListIterator;
import java.util.Locale;
import java.util.Map.Entry;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLHandshakeException;
import javax.net.ssl.SSLSocketFactory;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.SystemClock;
import android.util.Base64;
import android.util.Log;

import com.google.noconflictgson.Gson;
import com.google.noconflictgson.GsonBuilder;
import com.google.noconflictgson.JsonDeserializationContext;
import com.google.noconflictgson.JsonDeserializer;
import com.google.noconflictgson.JsonElement;
import com.google.noconflictgson.JsonParseException;
import com.google.noconflictgson.JsonParser;

/**
 * DO NOT USE FOR ANDROID VERSIONS < 2.2 (FROYO)
 * these versions will work, but connection pooling will be disabled so they will be quite inefficient
 */
public class EasyHttp
{
    /**
     * The SecureRandom vulnerability was due to Android 'reusing' Processes, without reseeding
     * This should only need to be called once per process, a good match for the static lifecycle
     */
    static {
        try {
            PRNGFixes.apply();
        }
        catch (Throwable e) {
            // I have seen the following exception on Android 4.1.2:
            // java.lang.SecurityException: SecureRandom.getInstance("SHA1PRNG") backed by wrong
            // Provider: class org.apache.harmony.security.provider.crypto.CryptoProvider
            e.printStackTrace();
        }
    }

    /** Class for EasyHttp configuration */
    public static class Config
    {
        /** Change to toggle additional print out */
        public volatile boolean debug = false;

        public final SSLInitialiser sslInitialiser;
        public final boolean followRedirects;
        public final int timeoutConnectAndSendMillis;
        public final int timeoutReadMillis;

        /** Used for an initial check to see if the phone is connected to a network. Can be null to bypass the connection check. */
        private final android.net.ConnectivityManager connectivityManager;

        /**
         * Convenience constructor with safe SSL, 15s connect timeout and 35s read timeout.
         * Enables "follow redirects", so requests with response code 3xx won't error unless there are too many redirects.
         *
         * @param context - Used for getSystemService(Context.CONNECTIVITY_SERVICE) and keeps a reference to the ConnectivityManager.
         */
        public Config(Context context)
        {
            this(false, true, (int) TimeUnit.SECONDS.toMillis(15), (int) TimeUnit.SECONDS.toMillis(35), context);
        }

        /**
         * Your Manifest requires ACCESS_NETWORK_STATE permission to show the "not connected to any network" error message
         * Pass null for the context parameter if you don't use this permission, to avoid stack traces in the logs
         *
         * @param allowInsecureSSL - Disables trusted certificate checks and introduces MITM attacks, not recommended for production use
         * @param followRedirects - Default false. True will allow 3xx redirects to be followed, even cross-protocol
         * @param timeoutConnectAndSendMillis - 15 * 1000 is reasonable, increase this if the devices connection is bad
         * @param timeoutReadMillis - 35 * 1000 is reasonable, increase this if the remote server is slow
         * @param context - Used for getSystemService(Context.CONNECTIVITY_SERVICE) and keeps a reference to the ConnectivityManager.
         */
        public Config(boolean allowInsecureSSL, boolean followRedirects, int timeoutConnectAndSendMillis, int timeoutReadMillis, Context context)
        {
            this((allowInsecureSSL ? EasyHttp.SSL_INITIALISER_DANGEROUS : null), followRedirects, timeoutConnectAndSendMillis, timeoutReadMillis, context);
        }

        /**
         * Your Manifest requires ACCESS_NETWORK_STATE permission to show the "not connected to any network" error message
         * Pass null for the context parameter if you don't use this permission, to avoid stack traces in the logs
         *
         * @param sslInitialiser - Allows you to configure SSL
         * @param followRedirects - Default false. True will allow 3xx redirects to be followed, even cross-protocol
         * @param timeoutConnectAndSendMillis - 15 * 1000 is reasonable, increase this if the devices connection is bad
         * @param timeoutReadMillis - 35 * 1000 is reasonable, increase this if the remote server is slow
         * @param context - Used for getSystemService(Context.CONNECTIVITY_SERVICE) and keeps a reference to the ConnectivityManager.
         */
        public Config(SSLInitialiser sslInitialiser, boolean followRedirects, int timeoutConnectAndSendMillis, int timeoutReadMillis, Context context)
        {
            this.sslInitialiser = sslInitialiser;
            this.followRedirects = followRedirects;
            this.timeoutConnectAndSendMillis = timeoutConnectAndSendMillis;
            this.timeoutReadMillis = timeoutReadMillis;
            this.connectivityManager = context == null ? null : initConnectivityManager(context);
        }

        private static ConnectivityManager initConnectivityManager(Context context)
        {
            try
            {
                return (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            }
            catch (Exception e)
            {
                Log.w("EasyHttp.Config", "Cannot get ConnectivityManager", e);
                return null;
            }
        }
    }

    /** Object with additional error related details you might want to use */
    public static class ErrorData
    {
        /**
         * True if the error type could possibly be fixed by retrying right now,
         * as opposed to some time further in the future
         */
        public final boolean retryCanFixError;

        /** Can be null, if present it gives extra detail about the error */
        public final Exception ex;

        /** Non-null readable message that can be given straight to the user */
        public final String readableMsg; // Always present

        /** Can be null, or the data the server returned if present */
        public final String errorStream;

        /**
         * @param retryCanFixError True if the error type could possibly be fixed by retrying right now,
         *                         as opposed to some time further in the future
         * @param ex Can be null, if present it gives extra detail about the error
         * @param readableMsg Non-null readable message that can be given straight to the user
         * @param errorStream Can be null, or the data the server returned if present
         */
        public ErrorData(boolean retryCanFixError, Exception ex, String readableMsg, String errorStream)
        {
            if (readableMsg == null) throw new IllegalArgumentException("readableMsg == null");
            this.retryCanFixError = retryCanFixError;
            this.ex = ex;
            this.readableMsg = readableMsg;
            this.errorStream = errorStream;
        }
    }

    /** Web call result, with failure or success, expected no data back from the server */
    public static class ResultNoData
    {
        /** True for a successful operation, False will have a non-null errorData object */
        public final boolean success;

        /** The response code or one of the following:
         * <br>
         * -1 if no valid response code was returned \n
         * <br>
         * RESPONSE_CODE_FAIL_SETUP on error during setup
         * <br>
         * RESPONSE_CODE_FAIL_SEND on error during connect, send, ack receive phases
         * <br>
         * RESPONSE_CODE_FAIL_RECEIVE on error during data receive phase
         * <br>
         * RESPONSE_CODE_FAIL_UNEXPECTED on error during any phase
         */
        public final int responseCode;

        /** Non-null when success == false */
        public final ErrorData errorData;

        /**
         * Construct a successful result
         *
         * @param responseCode any integer
         */
        public ResultNoData(int responseCode)
        {
            this.success = true;
            this.responseCode = responseCode;
            this.errorData = null;
        }

        /**
         * Construct a failure result.
         *
         * @param responseCode any integer
         * @param errorData non-null object with information about the error
         */
        public ResultNoData(int responseCode, ErrorData errorData)
        {
            this.success = false;
            this.responseCode = responseCode;
            this.errorData = errorData;
        }

        public void printStackTraceIfGiven()
        {
            if (success == false && errorData.ex != null) errorData.ex.printStackTrace();
        }

        public boolean hasExceptionMessage()
        {
            return success == false && errorData.ex != null && errorData.ex.getMessage() != null;
        }

        public boolean hasErrorStream()
        {
            return success == false && errorData.errorStream != null;
        }
    }

    /** Web call result, with failure or success, which guarantees has non-null data on success */
    public static class Result <T> extends ResultNoData
    {
        /** ALWAYS check success == true before using this field. (Always non-null on success) */
        public final T data;

        /**
         * Construct a successful result
         *
         * @param data non-null data to store
         * @param responseCode any integer
         */
        public Result(T data, int responseCode)
        {
            super(responseCode);
            if (data == null) throw new IllegalArgumentException("data == null");
            this.data = data;
        }

        /**
         * Construct a failure result.
         *
         * @param responseCode any integer
         * @param errorData non-null object with information about the error
         */
        public Result(int responseCode, ErrorData errorData)
        {
            super(responseCode, errorData);
            data = null;
        }
    }

    public enum InputDecision
    {
        /** The http call will end as a failure. An attempt will be made to read from the error stream */
        UNEXPECTED_CODE,
        /** onDoInput will be called (with a null stream) */
        PROCEED_NULL_STREAM,
        /** onDoInput will be called and the stream can be read from */
        PROCEED_OPEN_STREAM
    }

    public interface DoInput<T>
    {
        /**
         * Called after getInputDecision returns PROCEED_NULL_STREAM or PROCEED_OPEN_STREAM
         * Look at the responseCode and read from the stream to generate the result.
         *
         * @param con non-null connection object which you can use to optionally read cookies etc.
         * @param responseCode value as processed by getInputDecision
         * @param in null if getInputDecision returns PROCEED_NULL_STREAM
         *           non-null if getInputDecision returns PROCEED_OPEN_STREAM
         *           DO NOT CLOSE the stream, that will be done for you.
         *
         * @return NON-NULL object of type T
         */
        T onDoInput(HttpURLConnection con, int responseCode, BufferedInputStream in) throws IOException, NiceException;

        /**
         * Decide whether the response code is expected and how to proceed
         *
         * @param http connection object if you need to get the Config for example
         * @param responseCode the value as returned by con.getResponseCode()
         */
        InputDecision getInputDecision(EasyHttp http, int responseCode);
    }

    /** Base class, use it to customise the accepted response codes */
    public static class DoInputForNoData implements DoInput<Object>
    {
        private static final Object VOID = new Object();

        /** As there is no data, this implementation can accept any 200-series-code (i.e. 2xx) */
        @Override
        public InputDecision getInputDecision(EasyHttp http, int responseCode)
        {
            return responseCode >= 200 && responseCode < 300
                    ? InputDecision.PROCEED_NULL_STREAM
                    : InputDecision.UNEXPECTED_CODE;
        }

        /** @param in always null in this implementation */
        @Override
        public Object onDoInput(HttpURLConnection con, int responseCode, BufferedInputStream in)
        {
            return VOID;
        }
    }

    /** Base class, use it to customise the accepted response codes, and read the response data. */
    public static abstract class DoInputForData<T> implements DoInput<T>
    {
        @Override
        public InputDecision getInputDecision(EasyHttp http, int responseCode)
        {
            return responseCode == 200
                    ? InputDecision.PROCEED_OPEN_STREAM
                    : InputDecision.UNEXPECTED_CODE;
        }
    }

    /** Ready made InputHandlerForData for String responses */
    public static class DoInputForString extends DoInputForData<String>
    {
        private final String charset;

        /** @param charset passed to streamToString. The most common value is EasyHttp.CHARSET_UTF8 */
        public DoInputForString(String charset)
        {
            if (charset == null) throw new IllegalArgumentException("charset == null");
            this.charset = charset;
        }

        public String onDoInput(HttpURLConnection con, int responseCode, BufferedInputStream in) throws IOException
        {
            if (in == null) throw new IllegalArgumentException("Never happen case, InputDecision.PROCEED_OPEN_STREAM required");

            return streamToString(in, charset);
        }
    }

    /** Convenience version of DoInputForString using the UTF8 charset type */
    public static class DoInputForString_UTF8 extends DoInputForString
    {
        public DoInputForString_UTF8()
        {
            super(EasyHttp.CHARSET_UTF8);
        }
    }

    /**
     * Ready made InputHandlerForData for JSON-encoded responses which will be automatically
     * serialised to the class of your choice
     */
    public static class DoInputForJSON<T> extends DoInputForData<T>
    {
        private final String charset;
        private final Class<T> classOfT;
        private SimpleDateFormat simpleDateFormat = null;

        /**
         * @param charset passed to streamToString. The most common value is EasyHttp.CHARSET_UTF8
         * @param classOfT (non-anonymous) class type that should be created from the JSON (i.e. MyClass.class if you want an instance of MyClass)
         */
        public DoInputForJSON(String charset, Class<T> classOfT)
        {
            if (charset == null) throw new IllegalArgumentException("charset == null");
            this.charset = charset;
            this.classOfT = classOfT;
        }

        /**
         * Allows Gson to parse into Date objects, kind of like GsonBuilder.setDateFormat(String) except:
         * 1. Sets field to null on parse error
         * 2. UTC timezone is always used
         */
        public DoInputForJSON<T> setDateFormat_UTC(String pattern)
        {
            this.simpleDateFormat = new SimpleDateFormat(pattern, Locale.US);
            this.simpleDateFormat.setTimeZone(java.util.TimeZone.getTimeZone("UTC")); // Always present on Android, otherwise returns GMT if not found
            return this;
        }

        /**
         * Allows Gson to parse into Date objects, kind of like GsonBuilder.setDateFormat(String) except:
         * 1. Sets field to null on parse error
         * 2. The default timezone is always used
         */
        public DoInputForJSON<T> setDateFormat_LocalTime(String pattern)
        {
            this.simpleDateFormat = new SimpleDateFormat(pattern, Locale.US);
            this.simpleDateFormat.setTimeZone(java.util.TimeZone.getDefault());
            return this;
        }

        protected Gson getParser()
        {
            if (simpleDateFormat == null) return new Gson();
            else
            {
                return new GsonBuilder()
                    .registerTypeAdapter(java.util.Date.class, new JsonDeserializer<Date>()
                    {
                        @Override
                        public Date deserialize(JsonElement jsonElement, java.lang.reflect.Type type, JsonDeserializationContext jsonDeserializationContext) throws JsonParseException
                        {
                            try
                            {
                                return simpleDateFormat.parse(jsonElement.getAsString());
                            }
                            catch (java.text.ParseException e)
                            {
                                return null;
                            }
                        }
                    })
                    .create();
            }
        }

        public T onDoInput(HttpURLConnection con, int responseCode, BufferedInputStream in) throws IOException, NiceException
        {
            if (in == null) throw new IllegalArgumentException("Never happen case, InputDecision.PROCEED_OPEN_STREAM required");

            String text = streamToString(in, charset);

            try
            {
                // Logcat trims leading whitespace down to 4 spaces, so pretty print isn't good
                // https://code.google.com/p/android/issues/detail?id=75100#makechanges
                JsonElement je = new JsonParser().parse(text);
                String json = new GsonBuilder().setPrettyPrinting().create().toJson(je);
                Log.i("EasyHttp", json);
            }
            catch (com.google.noconflictgson.JsonParseException e)
            {
                Log.i("EasyHttp", "Formatted JSON Response Exception:");
                e.printStackTrace();
            }

            try
            {
                T parsedJsonObject = getParser().fromJson(text, classOfT);

                if (parsedJsonObject == null)
                {
                    // This can happen if parsing empty string with Gson 2.2.4
                    throw new NiceException(new ErrorData(true, null, "Unexpected empty response, please try again", text));
                }
                else
                {
                    return parsedJsonObject;
                }
            }
            catch (com.google.noconflictgson.JsonParseException e)
            {
                // Fixable as a cut off connection might render some JSON invalid
                e.printStackTrace();
                throw new NiceException(new ErrorData(true, e, "Error occurred while parsing JSON response", text));
            }
        }
    }

    /** Convenience version of DoInputForJSON using the UTF8 charset type */
    public static class DoInputForJSON_UTF8<T> extends DoInputForJSON<T>
    {
        /** @param classOfT (non-anonymous) class type that should be created from the JSON (i.e. MyClass.class if you want an instance of MyClass) */
        public DoInputForJSON_UTF8(Class<T> classOfT)
        {
            super(EasyHttp.CHARSET_UTF8, classOfT);
        }
    }

    /**
     * Ready made InputHandlerForData for reading a Bitmap from a url, and handles certain bugs on Android gracefully.
     * Specifically avoids Drawable.createFromStream for being far too slow, and avoids the fairly well known null-return bug on slow connections
     */
    public static final class DoInputForBitmap extends DoInputForData<Bitmap>
    {
        public Bitmap onDoInput(HttpURLConnection con, int responseCode, BufferedInputStream in) throws IOException, NiceException
        {
            if (in == null) throw new IllegalArgumentException("Never happen case, InputDecision.PROCEED_OPEN_STREAM required");

            BitmapFactoryDecodeInputStream bugFixIS = new BitmapFactoryDecodeInputStream(in);

            // This stream reading method does not scale the bitmap at all.
            // Do not use the non-deprecated method which takes resources, as that will scale based on screen density.
            // Obviously scaling an image up from its native dimensions just takes memory for no benefit.
            // It is akin to using BitmapFactory.Options.inScaled = false, except this actually works on all phones.
            Bitmap image = BitmapFactory.decodeStream(bugFixIS);

            if (image == null) throw new NiceException(new ErrorData(true, null, "Received data that could not be used to decode an image", null));
            else return image;
        }
    }

    /**
     * "FlushedInputStream", used by InputHandlerForBitmap to fix an Android bug (still necessary as of Jun 23 2013!!)
     * post #12 @ http://code.google.com/p/android/issues/detail?id=6066
     * BitmapFactory.decodeStream returns null (can occur on slow connections):
     * "Android's decoders do not currently support partial data on decode."
     * Note that you need a slow/hanging connection to reproduce the bug.
     */
    public static final class BitmapFactoryDecodeInputStream extends FilterInputStream
    {
        public BitmapFactoryDecodeInputStream(InputStream inputStream)
        {
            super(inputStream);
        }

        @Override
        public long skip(long n) throws IOException
        {
            long totalBytesSkipped = 0L;
            while (totalBytesSkipped < n)
            {
                long bytesSkipped = in.skip(n - totalBytesSkipped);
                if (bytesSkipped == 0L)
                {
                    int singleByte = read();
                    if (singleByte < 0)
                    {
                        break; // we reached EOF
                    }
                    else
                    {
                        bytesSkipped = 1; // we read one byte
                    }
                }
                totalBytesSkipped += bytesSkipped;
            }
            return totalBytesSkipped;
        }
    }

    // OUTPUT

    /** Base class for all output implementations */
    public static abstract class DoOutput
    {
        private final String outputContentType;

        /** @param outputContentType can be null to do nothing, or a string to be added as the Content-Type header */
        public DoOutput(String outputContentType)
        {
            this.outputContentType = outputContentType;
        }

        /**
         * You need to implement this to send text or data.
         * Throw DoOutputNiceException to have control over the resulting error message.
         * N.B. IOException has a detail-less error message. Any other Exception has a message
         * along the lines of "Error while converting the data to be sent, " + exception message if present + exception class name
         *
         * (You can also remove the throws Exception clause if you don't need it)
         */
        public abstract void onDoOutput(Config config, BufferedOutputStream out) throws IOException, NiceException;
    }

    /** Convenience class for writing a string to a stream */
    public static class DoOutputString extends DoOutput
    {
        private final String output;

        /**
         * @param outputContentType can be null to do nothing, or a string to be added as the Content-Type header
         * @param output can be null to send a 0 length string
         */
        public DoOutputString(String outputContentType, String output)
        {
            super(outputContentType);
            this.output = output;
        }

        /** {@inheritDoc} */
        @Override
        public void onDoOutput(Config config, BufferedOutputStream out) throws IOException
        {
            out.write((output == null ? "" : output).getBytes());
        }
    }

    /** Convenience version of DoOutputString using the UTF8 content type */
    public static class DoOutputString_UTF8 extends DoOutputString
    {
        /**
         * @param output can be null to send a 0 length string
         */
        public DoOutputString_UTF8(String output)
        {
            super(EasyHttp.CONTENT_TYPE_UTF8, output);
        }
    }

    /** Convenience class for writing an object to a stream as JSON */
    public static class DoOutputJSON extends DoOutput
    {
        private final Object toJsonObj;
        private final String alreadyJson;

        /**
         * @param outputContentType can be null to do nothing, or a string to be added as the Content-Type header
         * @param toJsonObj a plain String object will be output as-is.
         *                  a null object will send a 0 length string
         *                  a normal object will be converted to json with Gson() default parameters and then written as a string
         */
        public DoOutputJSON(String outputContentType, Object toJsonObj)
        {
            super(outputContentType);
            if (toJsonObj instanceof String)
            {
                this.toJsonObj = null;
                this.alreadyJson = (String) toJsonObj;
            }
            else
            {
                this.toJsonObj = toJsonObj;
                this.alreadyJson = null;
            }
        }

        /** {@inheritDoc} */
        @Override
        public void onDoOutput(Config config, BufferedOutputStream out) throws IOException, NiceException
        {
            String json = alreadyJson != null ? alreadyJson : toJsonObj == null ? "" : (new Gson().toJson(toJsonObj));
            out.write(json.getBytes());
        }
    }

    /** Convenience version of DoOutputJSON using the UTF8 content type */
    public static class DoOutputJSON_UTF8 extends DoOutputJSON
    {
        /**
         * @param toJsonObj a plain String object will be output as-is.
         *                  a null object will send a 0 length string
         *                  a normal object will be converted to json with Gson() default parameters and then written as a string
         */
        public DoOutputJSON_UTF8(Object toJsonObj)
        {
            super(EasyHttp.CONTENT_TYPE_JSON_UTF8, toJsonObj);
        }
    }

    /** Interface for writing a single multipart to a stream */
    public interface Multipart
    {
        /**
         * The multipart output should NOT contain any leading or trailing LINE_END characters.
         * The multipart should NOT close the output stream
         */
        void writePart(Config config, BufferedOutputStream out) throws IOException, NiceException;
    }

    /** Class for combining and writing out many multiparts. Allows you to watch progress. */
    public static final class DoOutputMultipart extends DoOutput
    {
        public interface OnPartUploadListener
        {
            void onStart(int partIndex);
        }

        private final static String BOUNDARY = "Boundary+0xAbCdEfGbOuNdArY";
        private final static String HYPHENS = "--";
        private final static String LINE_END = "\r\n";

        private static String getContentTypeWithBoundary()
        {
            return "multipart/form-data; boundary=" + BOUNDARY;
        }

        private final LinkedList<Multipart> parts = new LinkedList<Multipart>();
        private final OnPartUploadListener onPartUploadListener;

        public DoOutputMultipart()
        {
            this(null);
        }

        /** @param onPartUploadListener can be null to do nothing, or a string to be added as the Content-Type header */
        public DoOutputMultipart(OnPartUploadListener onPartUploadListener)
        {
            super(getContentTypeWithBoundary());
            this.onPartUploadListener = onPartUploadListener;
        }

        public void add(Multipart multipart)
        {
            if (multipart == null) throw new IllegalArgumentException("multipart == null");
            parts.add(multipart);
        }

        public void onDoOutput(Config config, BufferedOutputStream out) throws IOException, NiceException
        {
            final byte[] PART_BEGIN_BOUNDARY_BYTES = (HYPHENS + BOUNDARY + LINE_END).getBytes();
            final byte[] LINE_END_BYTES = LINE_END.getBytes();

            ListIterator<Multipart> iterator = parts.listIterator();

            int i = -1;
            while (iterator.hasNext())
            {
                i++;
                Multipart m = iterator.next();
                if (onPartUploadListener != null) onPartUploadListener.onStart(i);

                try
                {
                    out.write(PART_BEGIN_BOUNDARY_BYTES);
                    m.writePart(config, out);
                    out.write(LINE_END_BYTES);

                    // Ensure the BufferedOutputStream doesn't cache the load of writes, for onPartUploadListener to be accurate

                    // Flush all but the last multipart to avoid wasteful transmissions
                    // (last one will be flushed when this method returns, with the last boundary string also written)
                    if (iterator.hasNext()) out.flush();
                }
                catch (IOException e)
                {
                    Log.e("EasyHttp", "Error occurred while trying to send multipart (index " + i + ")");
                    throw e;
                }
            }

            out.write((HYPHENS + BOUNDARY + HYPHENS + LINE_END).getBytes());
        }

        public int countParts()
        {
            return parts.size();
        }
    }

    /** Class for writing a single string multipart to a stream */
    public static final class TextMultipart implements Multipart
    {
        private final String name;
        private final String content;

        public TextMultipart(String name, String content)
        {
            if (content == null) throw new IllegalArgumentException("content == null");

            this.name = name;
            this.content = content;
        }

        public void writePart(Config config, BufferedOutputStream out) throws IOException
        {
            out.write(("Content-Disposition: form-data; name=\"" + name + "\"\r\n\r\n").getBytes());
            out.write(content.getBytes());
        }
    }

    /** Class for writing a single file multipart to a stream */
    public static final class FileMultipart implements Multipart
    {
        public interface OnProgressChange
        {
            /**
             * callback from the background thread. If the file can't be found only the first onProgress
             * call will be given before the web call fails. This will be onProgress(0, 0).
             * @param totalSizeKB - total file size in KB, rounded to nearest KB. Will be 0 if there was some file error.
             * @param uploadedKB - total uploaded so far in KB, rounded to nearest KB. Always called with 0 before data is sent,
             *                     and if reported file size is correct, will always end with the same as totalSizeKB
             */
            void onProgress(int totalSizeKB, int uploadedKB);
        }

        // 4KB buffer. Will not load the whole file while uploading, only chunks of this much.
        private final static int BUFFER_SIZE = 4092;

        private final String name;
        private final String multipartFileName;
        private final String multipartMimeType;
        private final File file;
        private final OnProgressChange onProgressChange; // This can be null

        /**
         * @param name - e.g. without quotes "product_order_detail"
         * @param multipartFileName - e.g. without quotes "image.jpg"
         * @param multipartMimeType - e.g. without quotes "image/jpeg"
         * @param file - the file you wish to upload
         * @param onProgressChange - null if you don't want progress callbacks
         */
        public FileMultipart(String name, String multipartFileName, String multipartMimeType, File file, OnProgressChange onProgressChange)
        {
            if (name == null) throw new IllegalArgumentException("name == null");
            if (multipartFileName == null) throw new IllegalArgumentException("multipartFileName == null");
            if (multipartMimeType == null) throw new IllegalArgumentException("multipartMimeType == null");
            if (file == null) throw new IllegalArgumentException("file == null");
            // onProgressChange can be null

            this.name = name;
            this.multipartFileName = multipartFileName;
            this.multipartMimeType = multipartMimeType;
            this.file = file;
            this.onProgressChange = onProgressChange;
        }

        /**
         * multipartFileName will be set to file.getName()
         * @param name - e.g. without quotes "product_order_detail"
         * @param multipartMimeType - e.g. without quotes "image/jpeg"
         * @param file - the file you wish to upload
         * @param onProgressChange - null if you don't want progress callbacks
         */
        public FileMultipart(String name, String multipartMimeType, File file, OnProgressChange onProgressChange)
        {
            this(name, file == null ? "null" : file.getName(), multipartMimeType, file, onProgressChange);
        }

        public void writePart(Config config, BufferedOutputStream out) throws IOException, NiceException
        {
            out.write(("Content-Disposition: form-data; name=\"" + name + "\"; filename=\"" + multipartFileName + "\"\r\n").getBytes());
            out.write(("Content-Type: " + multipartMimeType + "\r\n\r\n").getBytes());

            byte[] buffer = new byte[BUFFER_SIZE];

            // Write image data
            InputStream is = null;
            boolean reading = true; // Used to differentiate file read IOException from output stream IOException
            try
            {
                is = new BufferedInputStream(new FileInputStream(file)); // Throws FileNotFoundException if the file doesn't exist
                int numReadBytes;

                // Prevent NullPointerException or DivideByZeroException
                if (onProgressChange == null)
                {
                    while ((numReadBytes = is.read(buffer)) > 0)
                    {
                        reading = false;
                        out.write(buffer, 0, numReadBytes);
                        reading = true;
                    }
                }
                else
                {
                    long fileSizeBytes = file.length();
                    if (fileSizeBytes == 0) fileSizeBytes = 1; // Avoid division by zero, probably will never happen

                    // (>>> 10) == (/ 1024), add 512 before the shift to essentially round the output, even though int division is used
                    int fileSizeKB = (int) ((fileSizeBytes + 512) >>> 10);

                    if (config.debug) Log.e("EasyHttp", "FileMultipart: " + fileSizeBytes + " Bytes");

                    // Calculate from the file size how often (in terms of bytes written out) we need to call onProgress,
                    // so that it will be called at most 100 times. Add 50 to round to nearest, e.g. 151 = called every 2b, 149 = called every 1b
                    long reportEveryXBytes = (fileSizeBytes + 50l) / 100l; // reportEvery can be 0 bytes

                    long totalReadBytes = 0;
                    long lastProgressTotalReadBytes = 0;
                    onProgressChange.onProgress(fileSizeKB, 0);

                    while ((numReadBytes = is.read(buffer)) > 0)
                    {
                        reading = false;
                        out.write(buffer, 0, numReadBytes);
                        reading = true;

                        // For a 400KB file (with 4KB cache) this calculation will only be performed 100 times
                        totalReadBytes += numReadBytes;

                        // This could potentially report slightly more frequently than once every percent complete
                        if (lastProgressTotalReadBytes + reportEveryXBytes < totalReadBytes)
                        {
                            lastProgressTotalReadBytes = totalReadBytes;

                            // (>>> 10) == (/ 1024), add 512 before the shift to essentially round the output, even though int division is used
                            int totalReadKB = (int) ((totalReadBytes + 512) >>> 10);

                            onProgressChange.onProgress(fileSizeKB, totalReadKB);
                        }
                    }
                }
            }
            catch (java.io.FileNotFoundException e)
            {
                throw new NiceException(false, e, "Upload failed, file not found. Part \"" + name + "\" from " + file.getAbsolutePath());
            }
            catch (IOException e)
            {
                // Give a nicer less generic message, to distinguish read and write (doOutput) IOExceptions
                if (reading)
                {
                    throw new NiceException(false, e, "Upload failed, cannot read file. Part \"" + name + "\" from " + file.getAbsolutePath());
                }
                else
                {
                    throw e;
                }
            }
            finally
            {
                try
                {
                    if (is != null) is.close();
                }
                catch (IOException e)
                {
                    // Upload actually might have succeeded... Log the error to avoid failing unnecessarily
                    Log.e("EasyHttp", "Ignored error, failed to close file stream for part \"" + name + "\" from " + file.getAbsolutePath());
                }
            }
        }
    }

    /** This is an API < 9 friendly alternative to java.util.AbstractMap.SimpleEntry */
    private static class SimpleEntryAPI0<K, V> implements java.util.Map.Entry<K, V>
    {
        private static boolean eq(Object o1, Object o2)
        {
            return o1 == null ? o2 == null : o1.equals(o2);
        }

        private final K key;
        private V value;

        public SimpleEntryAPI0(K key, V value)
        {
            this.key = key;
            this.value = value;
        }

        @Override
        public K getKey()
        {
            return key;
        }

        @Override
        public V getValue()
        {
            return value;
        }

        @Override
        public V setValue(V object)
        {
            V oldValue = this.value;
            this.value = object;
            return oldValue;
        }

        @Override
        public boolean equals(Object o)
        {
            if (!(o instanceof java.util.Map.Entry)) return false;
            java.util.Map.Entry<?, ?> e = (java.util.Map.Entry<?, ?>)o;
            return eq(key, e.getKey()) && eq(value, e.getValue());
        }

        @Override
        public int hashCode()
        {
            return (key   == null ? 0 :   key.hashCode()) ^
                   (value == null ? 0 : value.hashCode());
        }

        public String toString()
        {
            return key + "=" + value;
        }
    }

    /** Supported HTTP methods (I know the others are missing) */
    public enum Method
    {
        GET ("GET"),
        POST ("POST");

        public final String httpUrlConnectionMethod;

        private Method(String httpUrlConnectionMethod)
        {
            this.httpUrlConnectionMethod = httpUrlConnectionMethod;
        }
    }

    // -1 is reserved for con.getResponseCode() if no valid response code was given
    public final static int RESPONSE_CODE_FAIL_SETUP = -2;
    public final static int RESPONSE_CODE_FAIL_SEND = -3;
    public final static int RESPONSE_CODE_FAIL_RECEIVE = -4;
    public final static int RESPONSE_CODE_FAIL_UNEXPECTED = -5;

    public final static String CHARSET_UTF8 = "UTF-8";

    public final static String CONTENT_TYPE_UTF8 = "charset=utf-8";
    public final static String CONTENT_TYPE_JSON_UTF8 = "application/json" + "; " + CONTENT_TYPE_UTF8;
    public final static String CONTENT_TYPE_FORM_URLENCODED_UTF8 = "application/x-www-form-urlencoded";
    public final static String CONTENT_TYPE_MULTIPART_BASIC_BOUNDARY = "multipart/form-data; boundary=\"Boundary+0xAbCdEfGbOuNdArY\"";

    //! N.B. Make sure any default values here are restored with the reset() method

    /** This will be null if no params have been added. If non null this will start with ? and NOT have any trailing &. */
    private String urlQueryParams;
    private ArrayList<java.util.Map.Entry<String, String>> headers;
    private Config config;

    /** @param defaults Non-null configuration to use for this EasyHttp instance. */
    public EasyHttp(Config defaults)
    {
        if (defaults == null) throw new IllegalArgumentException("EasyHttp Config must be specified");

        reset(defaults);

        // HTTP connection reuse which was buggy pre-froyo
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.FROYO)
        {
            System.setProperty("http.keepAlive", "false");
        }
    }

    /** Resets everything as if you had created a new instance via "new EasyHttp()" */
    public void reset(Config defaults)
    {
        urlQueryParams = null;
        headers = null;
        config = defaults;
    }

    /**
     * This is used to add something in the format ?name=value&name2=value2 ... onto the url.
     * The names and values will be automatically encoded into URL safe characters by this method.
     * Adds onto existing params, if any.
     */
    public EasyHttp addParam(String name, String value)
    {
        try
        {
            String param = URLEncoder.encode(name, "UTF-8") + "=" + URLEncoder.encode(value, "UTF-8");

            // Add to the member variable, in the necessary form
            urlQueryParams = urlQueryParams == null ? ("?" + param) : (urlQueryParams + "&" + param);
        }
        catch (UnsupportedEncodingException e)
        {
            // UTF-8 should always be available
            throw new RuntimeException("never happen case", e);
        }

        return this;
    }

    /**
     * Adds another header to the request, which will see all keys in the order they are added.
     * If the same key is given multiple times, the last value to be added will be used.
     * @param key HTTP header key
     * @param value HTTP header value
     */
    public EasyHttp addHeader(String key, String value)
    {
        if (headers == null) headers = new ArrayList<java.util.Map.Entry<String, String>>();
        headers.add(new SimpleEntryAPI0<String, String>(key, value)); // Alternative to java.util.AbstractMap.SimpleEntry

        return this;
    }

    /**
     * Adds an 'Authorization' header to the request, with the given login and password
     * @param login account as a normal string, it will be Base64 encoded for you.
     * @param pass password as a normal string, it will be Base64 encoded for you.
     */
    public EasyHttp addAuthenticationBasic(String login, String pass)
    {
        byte[] auth = (login + ":" + pass).getBytes();
        String base64Auth = Base64.encodeToString(auth, Base64.NO_WRAP);
        addHeader("Authorization", "Basic " + base64Auth);

        return this;
    }

    /**
     * Adds an 'Authorization' header to the request, with the given bearer token
     * @param accessToken Bearer token to add to the request, 'Bearer ' will be added for you.
     */
    public EasyHttp addAuthenticationOAuth(String accessToken)
    {
        addHeader("Authorization", "Bearer " + accessToken);

        return this;
    }

    /** Interface for setting up SSL logic, can be used to call con.setSSLSocketFactory */
    public interface SSLInitialiser
    {
        void setup(HttpsURLConnection con) throws NiceException;
    }

    /**
     * Class for any exceptions that happen often enough to need a nice specific error message,
     * instead of a more generic one
     */
    public static class NiceException extends Exception
    {
        private static final long serialVersionUID = 1L;

        /** Non-null data used to create the failure Result object */
        public final ErrorData errorData;

        /**
         * @param retryCanFixError True if the error type could possibly be fixed by retrying right now,
         *                         as opposed to some time further in the future
         * @param ex Can be null, if present it gives extra detail about the error
         * @param msg Non-null readable message that can be given straight to the user
         */
        public NiceException(boolean retryCanFixError, Exception ex, String msg)
        {
            this(new ErrorData(retryCanFixError, ex, msg, null));
        }

        /**
         * @param errorData Non-null data used to create the failure Result object
         */
        public NiceException(ErrorData errorData)
        {
            super(errorData == null ? "" : errorData.readableMsg);

            if (errorData == null) throw new IllegalArgumentException();

            this.errorData = errorData;
        }

        /**
         * Returns a new NiceException that you can throw.
         * It has the correct error message for an exception thrown from the SSLSocketFactoryHelper class
         */
        public static NiceException fromSSLSocketFactoryHelper(KeyManagementException e)
        {
            return new NiceException(false, e, "Unexpected SSL Key Error during SSLContext init: " + (e.getMessage() == null ? "" : e.getMessage()));
        }

        /**
         * Returns a new NiceException that you can throw.
         * It has the correct error message for an exception thrown from the SSLSocketFactoryHelper class
         */
        public static NiceException fromSSLSocketFactoryHelper(NoSuchAlgorithmException e)
        {
            return new NiceException(false, e, "Requested SSL protocol was not found");
        }
    }

    /**
     * @param e The exception thrown while attempting to write output
     * @return A better exception with a readable exception message, depending on the type of IOException
     */
    private NiceException newNiceExceptionFromDoOutput(IOException e)
    {
        if (e instanceof SSLHandshakeException) {
            String extraHelp = config.debug && ! isDefaultDangerousSSLInitialiser(config.sslInitialiser)
                    ? " (see Config.allowInsecureSSL)"
                    : "";

            return new NiceException(true, e, "SSL Handshake Failed. Perhaps the certificate is untrusted?" + extraHelp);
        }
        else if (e instanceof SocketTimeoutException)
        {
            return new NiceException(true, e, "Timeout while trying to send request");
        }
        else if (isOutputNetworkDisconnectedException(e))
        {
            // This happens (no SocketTimeoutException) if another defective app disconnects the network while the call was in progress.
            // In that case, 1. Create socket 2. Put device to sleep 3. Wait one or two minutes 4. java.net.SocketException: ETIMEDOUT
            // if (true) throw new java.net.SocketException("sendto failed: ETIMEDOUT (Connection timed out)");
            return new NiceException(true, e, "Network disconnected while trying to send request");
        }
        else
        {
            return new NiceException(true, e, "Error occurred while trying to send request");
        }
    }

    /**
     * This gives the correct error message for an exception thrown by con.getInputStream() or
     * con.getResponseCode() - which calls getInputStream internally.
     * @param e The exception thrown while attempting to read input.
     * @return A better exception with a readable exception message, depending on the type of IOException
     */
    private NiceException newNiceExceptionFromGetInputStream(IOException e)
    {
        if (e instanceof SocketTimeoutException)
        {
            return new NiceException(true, e, "Timeout while trying to read response");
        }
        else if (e instanceof SSLHandshakeException)
        {
            String extraHelp = config.debug && !isDefaultDangerousSSLInitialiser(config.sslInitialiser)
                    ? " (see Config.allowInsecureSSL)"
                    : "";

            return new NiceException(true, e, "SSL Handshake Failed. Perhaps the certificate is untrusted?" + extraHelp);
        }
        else
        {
            return new NiceException(true, e, "Error occurred while trying to read response");
        }
    }

    /** SSLInitialiser which will accept any certificate, even if self signed. */
    public static final SSLInitialiser SSL_INITIALISER_DANGEROUS = new SSLInitialiser()
    {
        @Override
        public void setup(HttpsURLConnection con) throws NiceException
        {
            try
            {
                SSLSocketFactory sf = SSLSocketFactoryHelper.newInstanceAcceptAllCerts(SSLSocketFactoryHelper.PROTOCOL_SSL);
                con.setSSLSocketFactory(sf);
            }
            catch (KeyManagementException e)
            {
                throw NiceException.fromSSLSocketFactoryHelper(e);
            }
            catch (NoSuchAlgorithmException e)
            {
                throw NiceException.fromSSLSocketFactoryHelper(e);
            }

            con.setHostnameVerifier(new AcceptAllHostnameVerifier());
        }
    };

    /** @return True if the given SSLInitialiser is the default dangerous one (trusts anything) */
    private static final boolean isDefaultDangerousSSLInitialiser(SSLInitialiser initialiser) {
        return initialiser == SSL_INITIALISER_DANGEROUS;
    }

    /**
     * @return True if there is a usable network and it is connected. Type of network doesn't matter
     *         False if we know a network request is going to fail for sure
     */
    private static boolean isNetworkConnected(Config config)
    {
        try
        {
            if (config.connectivityManager == null) return true;

            // Requires ACCESS_NETWORK_STATE permission
            NetworkInfo networkInfo = config.connectivityManager.getActiveNetworkInfo();
            return (networkInfo != null && networkInfo.isConnected());
        }
        catch (Exception e)
        {
            e.printStackTrace();
            return true;
        }
    }

    /**
     * @return True if the given IOException is because the network was disconnected. This may not
     *         work perfectly for future Android versions
     */
    private static boolean isOutputNetworkDisconnectedException(IOException e)
    {
        // This is different from the usual java.net.SocketTimeoutException
        return e instanceof java.net.SocketException && e.getMessage() != null && e.getMessage().matches("(?i)sendto failed.*ETIMEDOUT.*");
    }

    /**
     * Reads the input stream to its completion
     * N.B. This doesn't close the given input stream
     * N.B. You have to make sure the line endings are consistent (this does not handle CR/LF translations too well)
     * @param in InputStream to read from
     * @param charset Normally "UTF-8"
     *
     * @return NON-NULL data that is read from the stream
     */
    public static String streamToString(InputStream in, String charset) throws IOException
    {
        // From "Stupid Scanner tricks" article. The reason it works is because Scanner iterates over tokens
        // in the stream, and in this case we separate tokens using "beginning of the input boundary" (\A)
        // thus giving us only one token for the entire contents of the stream.
        // This approach does not handle CR/LF translations too well.
        // So you have to make sure the line endings are consistent.

        // Because "in" is closed properly elsewhere and nobody else has a handle
        // to this scanner object, there is no reason to close this scanner.
        // NB, if you do call close on the scanner it will close the InputStream as well
        @SuppressWarnings("resource")
        java.util.Scanner s = new java.util.Scanner(in, charset);
        s.useDelimiter("\\A");

        String responseStr = s.hasNext() ? s.next() : "";
        return responseStr;
    }

    private class RetryRequiredException extends Exception
    { }

    private static class RedirectHandle
    {
        private final Method originalMethod;
        private final URL originalURL;
        private final Method newMethod;
        private final URL newURL;

        public RedirectHandle(Method originalMethod, URL originalURL, Method newMethod, URL newURL)
        {
            this.originalMethod = originalMethod;
            this.originalURL = originalURL;
            this.newMethod = newMethod;
            this.newURL = newURL;
        }

        public String getNewUrl()
        {
            return newURL.toExternalForm();
        }

        public Method getNewMethod()
        {
            return newMethod;
        }

        public boolean isProtocolUnchanged()
        {
            if (originalURL.getProtocol() == null) return newURL.getProtocol() == null;
            else return originalURL.getProtocol().equals(newURL.getProtocol());
        }

        public boolean isMethodUnchanged()
        {
            return originalMethod == newMethod;
        }
    }

    private URL getRedirectUrl(URL originalURL, HttpURLConnection con) throws MalformedURLException
    {
        String location = con.getHeaderField("Location");

        if (location == null)
        {
            throw new MalformedURLException("\"Location\" header not specified");
        }

        // con.getURL() is NOT the same as originalURL
        return new URL(originalURL, location);
    }

    /**
     * @return null if no redirect work-around is required
     *         non-null if a redirect needs to be performed (because HttpUrlConnection won't do it)
     */
    private RedirectHandle getRedirectWorkaround(Method method, URL originalURL, HttpURLConnection con, int responseCode)
    {
        try
        {
            final RedirectHandle rh;

            switch (responseCode)
            {
                case 300:
                case 304:
                case 305:
                case 306:
                default:
                    // Types we don't support
                    rh = null;
                    break;
                case 301:
                case 302:
                case 307:
                case 308:
                    // Location header present
                    // No change in method
                    rh = new RedirectHandle(method, originalURL, method, getRedirectUrl(originalURL, con));
                    break;
                case 303:
                    // Location header present
                    // New method must be GET
                    rh = new RedirectHandle(method, originalURL, Method.GET, getRedirectUrl(originalURL, con));
                    break;
            }

            // HttpUrlConnection will fail if either of these have changed
            return rh != null && rh.isMethodUnchanged() && rh.isProtocolUnchanged() ? null : rh;
        }
        catch (MalformedURLException e)
        {
            // getRedirectUrl parse failure
            Log.w("EasyHttp", "Suppressing HTTP " + responseCode + " redirect, failed to get redirect URL");
            return null;
        }
    }

    /**
     * Private helper method, to make the casting a bit more obvious. Avoids warnings where T is unknown.
     * The only thing this method really needs to do is differentiate between Result<?> and ResultNoData, as the <> is thrown away at compile time
     */
    private static Result<Object> newErrorResult(int responseCode, ErrorData errorData)
    {
        if (errorData.ex != null) errorData.ex.printStackTrace();

        return new Result<Object>(responseCode, errorData);
    }

    /** Private helper method, to throw a nice exception if data is null or invalid, as returned from onDoInput */
    private static <T> Result<T> newResultFromOnDoInput(int responseCode, T data) throws NiceException
    {
        if (data == null) throw new NiceException(false, new NullPointerException(), "onDoInput cannot return null");

        return new Result<T>(data, responseCode);
    }

    /**
     * Call a URL, send nothing, expect nothing back
     * @return Result object that is NEVER null
     * @param urlStr URL to connect to
     * @param method EasyHttp.(GET or POST)
     */
    public ResultNoData runWantNoResult(String urlStr, Method method)
    {
        final DoOutput doOutputImpl = null;

        return (ResultNoData) run(urlStr, method, doOutputImpl, new DoInputForNoData());
    }

    /**
     * Call a URL, send some data, expect nothing back
     * @return Result object that is NEVER null
     * @param urlStr URL to connect to
     * @param method EasyHttp.(GET or POST)
     * @param output DoOutput object that will write what you want to the output stream
     */
    public ResultNoData runWantNoResult(String urlStr, Method method, DoOutput output)
    {
        if (output == null) throw new IllegalArgumentException("output == null");

        return (ResultNoData) run(urlStr, method, output, new DoInputForNoData());
    }

    /**
     * Call a URL, send nothing, expect a String response
     * @return Result object that is NEVER null
     * @param urlStr URL to connect to
     * @param method EasyHttp.(GET or POST)
     */
    public Result<String> runWantString(String urlStr, Method method)
    {
        return runWantObject(urlStr, method, new DoInputForString_UTF8());
    }

    /**
     * Call a URL, send some data, expect a String response
     * @return Result object that is NEVER null
     * @param urlStr URL to connect to
     * @param method EasyHttp.(GET or POST)
     * @param output DoOutput object that will write what you want to the output stream
     */
    public Result<String> runWantString(String urlStr, Method method, DoOutput output)
    {
        return runWantObject(urlStr, method, output, new DoInputForString_UTF8());
    }

    /**
     * Call a URL, send nothing, expect a String response
     * EasyHttp method will always be GET. You can make an instance of InputHandlerForBitmap class to do something else if you need to.
     * @return Result object that is NEVER null
     * @param urlStr URL to connect to
     */
    public Result<Bitmap> runWantBitmap(String urlStr)
    {
        return runWantObject(urlStr, Method.GET, new DoInputForBitmap());
    }

    /**
     * Call a URL, send nothing, expect a JSON response
     * @return Result object that is NEVER null
     * @param classOfT Dynamically inflate an instance of this class with Gson, and read the JSON response into matching named (or annotated) fields.
     *                 The response will fail with a parse error if the response can't be parsed as JSON
     *                 Fields in the dynamically inflated class are silently left null (or default value) if such fields don't exist in the response
     *                 The fields names in the dynamically inflated class need to match the response fields, or annotate each field with @SerializedName ("FieldName")
     * @param urlStr URL to connect to
     * @param method EasyHttp.(GET or POST)
     */
    public <T> Result<T> runWantJSON(Class<T> classOfT, String urlStr, Method method)
    {
        return runWantObject(urlStr, method, new DoInputForJSON_UTF8<T>(classOfT));
    }

    /**
     * Call a URL, send some data, expect a JSON response
     * @return Result object that is NEVER null
     * @param classOfT Dynamically inflate an instance of this class with Gson, and read the JSON response into matching named (or annotated) fields.
     *                 The response will fail with a parse error if the response can't be parsed as JSON
     *                 Fields in the dynamically inflated class are silently left null (or default value) if such fields don't exist in the response
     *                 The fields names in the dynamically inflated class need to match the response fields, or annotate each field with @SerializedName ("FieldName")
     * @param urlStr URL to connect to
     * @param method EasyHttp.(GET or POST)
     * @param output DoOutput object that will write what you want to the output stream
     */
    public <T> Result<T> runWantJSON(Class<T> classOfT, String urlStr, Method method, DoOutput output)
    {
        return runWantObject(urlStr, method, output, new DoInputForJSON_UTF8<T>(classOfT));
    }

    /**
     * Use this if you need more data than just the JSON Object returned by runWantJSON#Class<T>, String, Method
     * (you can extend EasyHttp.InputHandlerForJSONUTF8 and pass that in if you wish to get additional data like cookies or something)
     * @return Whatever your inputHandler returns, or a guaranteed non-null failed result
     * @param urlStr URL to connect to
     * @param method EasyHttp.(GET or POST)
     * @param inputHandler Your input handler which generates the response object you are returned
     */
    @SuppressWarnings("unchecked")
    public <T> Result<T> runWantObject(String urlStr, Method method, DoInputForData<T> inputHandler)
    {
        if (inputHandler == null) throw new IllegalArgumentException("inputHandler == null");

        final DoOutput outputImpl = null;

        return (Result<T>) run(urlStr, method, outputImpl, inputHandler);
    }

    /**
     * Use this if you need more data than just the JSON Object returned by runWantJSON#Class<T>, String, Method, String, DoOutput
     * (you can extend EasyHttp.InputHandlerForJSONUTF8 and pass that in if you wish to get additional data like cookies or something)
     * @return Whatever your inputHandler returns, or a guaranteed non-null failed result
     * @param urlStr URL to connect to
     * @param method EasyHttp.(GET or POST)
     * @param output DoOutput object that will write what you want to the output stream
     * @param inputHandler Your input handler which generates the response object you are returned
     */
    @SuppressWarnings("unchecked")
    public <T> Result<T> runWantObject(String urlStr, Method method, DoOutput output, DoInputForData<T> inputHandler)
    {
        if (output == null) throw new IllegalArgumentException("output == null");
        if (inputHandler == null) throw new IllegalArgumentException("inputHandler == null");

        return (Result<T>) run(urlStr, method, output, inputHandler);
    }

    /**
     * The function that actually does stuff
     *
     * @param urlStr The url to connect to. MUST BE HTTP OR HTTPS.
     * @param method GET or POST connection method
     * @param doOutputImpl object which will write the output, or null if not writing anything out
     * @param inputHandler a handler to determine what to do with input (or anything at all)
     *
     * @return Object that is never null. An instance of either ResultNoData, Result<String> or Result<JsonType> depending on ResultType
     */
    private Result<?> run(String urlStr, Method method, DoOutput doOutputImpl, DoInput<?> inputHandler)
    {
        return run(urlStr, method, doOutputImpl, inputHandler, 0, 0);
    }

    /** Part 1 */
    private URL getURLWithQuery(String urlStr) throws NiceException
    {
        try
        {
            return new URL(urlStr + (urlQueryParams == null ? "" : urlQueryParams));
        }
        catch (MalformedURLException e)
        {
            throw new NiceException(false, e, "The connection URL is malformed");
        }
    }

    /** Part 2 */
    private HttpURLConnection openConnection(URL url) throws NiceException
    {
        try
        {
            return (HttpURLConnection) url.openConnection();
        }
        catch (IOException e)
        {
            throw new NiceException(true, e, "Error occurred while acquiring the connection");
        }
        catch (java.lang.ClassCastException e)
        {
            throw new NiceException(false, e, "The connection URL is not Http");
        }
    }

    /** Part 3 */
    private void setupConnection(HttpURLConnection con, Method method, DoOutput doOutputImpl) throws NiceException
    {
        if (con instanceof HttpsURLConnection)
        {
            HttpsURLConnection httpsCon = (HttpsURLConnection) con;
            if (config.sslInitialiser != null) config.sslInitialiser.setup(httpsCon);
        }

        try
        {
            con.setRequestMethod(method.httpUrlConnectionMethod);
        }
        catch (ProtocolException e)
        {
            throw new NiceException(false, e, "The method is not supported by this HTTP implementation");
        }

        con.setConnectTimeout(config.timeoutConnectAndSendMillis);
        con.setReadTimeout(config.timeoutReadMillis);
        con.setInstanceFollowRedirects(config.followRedirects);

        // Content Type for the sent data
        if (doOutputImpl != null && doOutputImpl.outputContentType != null) addHeader("Content-Type", doOutputImpl.outputContentType);

        if (headers != null)
        {
            for (Entry<String, String> e : headers)
            {
                // Add headers before the connection is attempted
                con.setRequestProperty(e.getKey(), e.getValue());
            }
        }

        con.setDoInput(true); // We want to read at least a response code
        con.setDoOutput(doOutputImpl != null);

        // Either call setChunkedStreamingMode(0) for default chunk size (HttpEngine.DEFAULT_CHUNK_LENGTH = 1024) or setFixedLengthStreamingMode(int)
        // Patryk doesn't like the 0 setting. The default is 1024 normally anyway.
        // if (doOutput) con.setChunkedStreamingMode(0);
        if (doOutputImpl != null) con.setChunkedStreamingMode(1024);
    }

    /** Part 4 Output Stream */
    private void doOutput(HttpURLConnection con, DoOutput doOutputImpl) throws NiceException
    {
        BufferedOutputStream out = null;
        try
        {
            out = new BufferedOutputStream(con.getOutputStream());
            doOutputImpl.onDoOutput(config, out);
        }
        catch (IOException e) // this includes SSLHandshakeException and SocketTimeoutException
        {
            throw newNiceExceptionFromDoOutput(e);
        }
        catch (Exception e)
        {
            // Anything else that was thrown by client code when onDoOutput was called, make a nice default message for them
            String eClass = e.getClass().getName();
            String message = e.getMessage() == null ? eClass : (e.getMessage() + ": " + eClass);

            throw new NiceException(false, e, "Error sending request data - " + message);
        }
        finally
        {
            try
            {
                if (out != null) out.close();
            }
            catch (IOException e)
            {
                throw new NiceException(true, e, "Error occurred while closing send request");
            }
        }
    }

    /** Part 5 Input Stream For Headers */
    private int readResponseCode(HttpURLConnection con, long startInputElapsedMillis, int numRetries) throws NiceException, RetryRequiredException
    {
        try
        {
            // Check response code before calling con.getInputStream() as that will fully timeout when there was an error
            // (i.e. anything not 2xx, 200 <= code < 300 means a successful response)
            // -1 response code indicates "No valid response code"
            return con.getResponseCode();
        }
        catch (java.io.EOFException e)
        {
            // EOF is probably due to an older android bug reusing a connection where it shouldn't.
            // A single retry is enough to avoid the EOF on our Galaxy S3

            // Time measured from "BufferedInputStream in" on the Galaxy S3 is normally very short (0-2ms)
            final long inputElapsedMillis = SystemClock.elapsedRealtime() - startInputElapsedMillis;
            final boolean hasTimeForAnotherRetry = inputElapsedMillis < (float) config.timeoutReadMillis * 0.1f;

            if (hasTimeForAnotherRetry && (numRetries == 0 || numRetries == 1))
            {
                // Try 2 more times, the first time fixes everything as far as I know
                throw new RetryRequiredException();
            }
            else if (hasTimeForAnotherRetry == false)
            {
                // EOF, where the input took a long time. I havn't seen this happen.
                String plural = numRetries == 0 ? "" : "s";
                throw new NiceException(true, e, "Response" + plural + " didn't complete. Please try again or turn off any network proxies.");
            }
            else
            {
                // EOF, both retries failed (quickly), this is the android bug we were trying to work around.
                String plural = numRetries == 0 ? "" : "s";
                throw new NiceException(true, e, "Response" + plural + " ended unexpectedly. Please try again.");
            }
        }
        catch (IOException e)
        {
            throw newNiceExceptionFromGetInputStream(e);
        }
    }

    /** Part 6 Input Stream For Content */
    private <T> Result<T> readInput(HttpURLConnection con, URL url, Method method, DoInput<T> inputHandler, int responseCode) throws NiceException
    {
        // Recieve input. When doInput == false, in will remain null
        BufferedInputStream in = null;
        try
        {
            // If the server returns 400 code for example, con.getInputStream() will time out
            // Detect this ahead of time
            InputDecision inputDecision = inputHandler.getInputDecision(EasyHttp.this, responseCode);

            if (inputDecision == InputDecision.PROCEED_OPEN_STREAM)
            {
                // Modes which want to process input must get a non-null stream
                in = new BufferedInputStream(con.getInputStream());
                T data = inputHandler.onDoInput(con, responseCode, in);
                return newResultFromOnDoInput(responseCode, data);
            }
            else if (inputDecision == InputDecision.PROCEED_NULL_STREAM)
            {
                T data = inputHandler.onDoInput(con, responseCode, null);
                return newResultFromOnDoInput(responseCode, data);
            }
            else if (inputDecision == InputDecision.UNEXPECTED_CODE)
            {
                throw newNiceExceptionFromErrorStream(con, responseCode);
            }
            else
            {
                throw new RuntimeException("Unexpected case");
            }
        }
        catch (IOException e)
        {
            throw newNiceExceptionFromGetInputStream(e);
        }
        finally
        {
            try
            {
                if (in != null) in.close();
            }
            catch (IOException e)
            {
                throw new NiceException(true, e, "Error occurred while closing read response");
            }
        }
    }

    /** Part 6b Error Stream */
    private NiceException newNiceExceptionFromErrorStream(HttpURLConnection con, int responseCode)
    {
        String message;
        try
        {
            message = con.getResponseMessage();
        }
        catch (IOException e)
        {
            message = null;
        }
        message = message == null ? "" : (", " + message);

        // This method will not cause a connection to be initiated. If the connection was not connected, or if the
        // server did not have an error while connecting or if the server had an error but no error data was sent,
        // this method will return null. This is the default.
        InputStream errorIsOrNull = con.getErrorStream();

        if (errorIsOrNull != null)
        {
            // Try to get an error message from the error stream
            BufferedInputStream in = new BufferedInputStream(errorIsOrNull);
            try
            {
                // Add getErrorStream() contents to message
                String text = streamToString(in, CHARSET_UTF8);
                String errorStream = text.length() == 0 ? null : text;

                return new NiceException(new ErrorData(false, null, "Error " + responseCode + message, errorStream));
            }
            catch (IOException e)
            {
                e.printStackTrace();

                // We tried to add something to the error message, but no lucks
                // IOException here means there will probably be one when stream attempts to close,
                // avoid that as we only wanted a little extra error message.
                try { in.close(); }
                catch (IOException e2) { }
            }
        }

        // Return a message with the responseCode, message and errorStream contents
        return new NiceException(new ErrorData(false, null, "Error " + responseCode + message, null));
    }

    /**
     * Same as the other run method, but used to support a variable number of retries
     * @param numRetries should start off as 0
     */
    private Result<?> run(
            String urlStr,
            Method method,
            DoOutput doOutputImpl,
            DoInput<?> inputHandler,
            int numRetries,
            int numRedirects)
    {
        // http://www.freesoft.org/CIE/RFC/1945/46.htm
        // A user agent should never automatically redirect a request more than 5 times, since such redirections usually indicate an infinite loop.
        if (numRedirects > 5)
        {
            return newErrorResult(RESPONSE_CODE_FAIL_UNEXPECTED, new ErrorData(false, null, "Too many redirects detected (" + numRedirects + ")", null));
        }

        HttpURLConnection con = null;
        try
        {
            if (inputHandler == null)
            {
                throw new IllegalArgumentException();
            }

            if (isNetworkConnected(config) == false)
            {
                return newErrorResult(RESPONSE_CODE_FAIL_SETUP, new ErrorData(true, null, "Your device is not connected to a network", null));
            }

            final URL url;

            // Open
            try
            {
                url = getURLWithQuery(urlStr);

                con = openConnection(url);

                setupConnection(con, method, doOutputImpl);
            }
            catch (NiceException e)
            {
                return newErrorResult(RESPONSE_CODE_FAIL_SETUP, e.errorData);
            }

            // Write
            try
            {
                if (doOutputImpl != null)
                {
                    doOutput(con, doOutputImpl);
                }
            }
            catch (NiceException e)
            {
                return newErrorResult(RESPONSE_CODE_FAIL_SEND, e.errorData);
            }

            // Read from first bit of input (headers and response code)
            final long startInputElapsedMillis = SystemClock.elapsedRealtime();
            final int responseCode;
            try
            {
                responseCode = readResponseCode(con, startInputElapsedMillis, numRetries);
            }
            catch (RetryRequiredException e)
            {
                return run(urlStr, method, doOutputImpl, inputHandler, numRetries + 1, numRedirects);
            }
            catch (NiceException e)
            {
                return newErrorResult(RESPONSE_CODE_FAIL_RECEIVE, e.errorData);
            }

            // Follow any redirect manually to work around "feature" of HttpUrlConnection
            if (config.followRedirects)
            {
                RedirectHandle redirect = getRedirectWorkaround(method, url, con, responseCode);
                if (redirect != null)
                {
                    return run(redirect.getNewUrl(), redirect.getNewMethod(), doOutputImpl, inputHandler, 0, numRedirects + 1);
                }
            }

            // Read from content part of input
            try
            {
                return readInput(con, url, method, inputHandler, responseCode);
            }
            catch (NiceException e)
            {
                return newErrorResult(responseCode, e.errorData);
            }
        }
        catch (Exception e)
        {
            String message = e.getMessage() == null ? "" : e.getMessage();

            return newErrorResult(RESPONSE_CODE_FAIL_UNEXPECTED, new ErrorData(true, e, "Unexpected error: " + message, null));
        }
        catch (java.lang.OutOfMemoryError e)
        {
            // Give the user a heads up, GC so we hopefully have enough space for the result object
            System.gc();
            return newErrorResult(RESPONSE_CODE_FAIL_UNEXPECTED, new ErrorData(false, null, "Out of memory: Crash imminent. Please force restart the app", null));
        }
        finally
        {
            if (con != null) con.disconnect();
        }
    }
}
