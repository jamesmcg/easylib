package com.jamesmcg.easylib.web;

import java.math.BigInteger;
import java.security.KeyStore;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.security.interfaces.RSAPublicKey;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509TrustManager;

/**
 * Reference https://www.owasp.org/index.php/Pinning_Cheat_Sheet
 * Code from https://www.owasp.org/images/1/1f/Pubkey-pin-android.zip
 *
 * Original code has been modified.
 *
 * Many thanks to Nikolay Elenkov for feedback.
 * Shamelessly based upon Moxie's example code (AOSP/Google did not offer code)
 * http://www.thoughtcrime.org/blog/authenticity-is-broken-in-ssl-but-your-app-ha/
 */
public class TrustManagerKeyPinner implements X509TrustManager {
    private static final List<String> supportedRSATypesUpper = Collections.unmodifiableList(
            Arrays.asList(
                    /**
                     * The server public key must be RSA, authTypeUpper.contains("RSA") WON'T WORK,
                     * because for some types chain[0].getPublicKey() isn't RSA. Notable examples
                     * are "ECDH_RSA", "DH_RSA" where the server public key is a Diffie-Hellman key.
                     * Others with RSA in their name: "ECDH_RSA", "DH_RSA", "SRP_SHA_RSA"
                     */
                    new String[] { "ECDHE_RSA", "RSA", "DHE_RSA" }
            )
    );

    private final String publicKey;

    /**
     * @param publicKey DER public key in hex encoding (0-9 a-f A-F)
     *                  See the Base64 encoded key using:
     *                  openssl x509 -inform DER -in FP_Certificate.cer -pubkey -noout
     *                  Get the hex encoded content of the key (no header and footer) using:
     *                  http://tomeko.net/online_tools/base64.php?lang=en
     */
    public TrustManagerKeyPinner(String publicKey) {
        if (publicKey.startsWith("0x")) {
            throw new IllegalArgumentException("Remove 0x from the start of publicKey");
        }
        if (publicKey.length() == 0) {
            throw new IllegalArgumentException("publicKey should not be empty");
        }
        if ( ! publicKey.matches("[0-9a-fA-F]+")) {
            throw new IllegalArgumentException("Illegal character(s) - Hex format required (0-9 a-f A-F)");
        }

        this.publicKey = publicKey;
    }

    public void checkServerTrusted(X509Certificate[] chain, String authType)
            throws CertificateException {

        assert (chain != null);
        if (chain == null) {
            throw new IllegalArgumentException("checkServerTrusted: X509Certificate array is null");
        }

        assert (chain.length > 0);
        if (!(chain.length > 0)) {
            throw new IllegalArgumentException("checkServerTrusted: X509Certificate is empty");
        }

        boolean isTypeSupported = (null != authType)
                && supportedRSATypesUpper.contains(authType.toUpperCase(Locale.ENGLISH));

        assert (isTypeSupported);
        if ( ! isTypeSupported) {
            throw new CertificateException("checkServerTrusted: AuthType must use RSA, it is " + authType);
        }

        // Perform customary SSL/TLS checks
        TrustManagerFactory tmf;
        try {
            tmf = TrustManagerFactory.getInstance("X509");
            tmf.init((KeyStore) null);

            for (TrustManager trustManager : tmf.getTrustManagers()) {
                ((X509TrustManager) trustManager).checkServerTrusted(
                        chain, authType);
            }

        } catch (Exception e) {
            throw new CertificateException(e);
        }

        // Hack ahead: BigInteger and toString(). We know a DER encoded Public
        // Key starts with 0x30 (ASN.1 SEQUENCE and CONSTRUCTED), so there is
        // no leading 0x00 to drop.
        RSAPublicKey pubkey = (RSAPublicKey) chain[0].getPublicKey();
        String encoded = new BigInteger(1 /* positive */, pubkey.getEncoded())
                .toString(16);

        // Pin it!
        final boolean expected = publicKey.equalsIgnoreCase(encoded);
        assert(expected);
        if (!expected) {
            throw new CertificateException("checkServerTrusted: Expected public key: "
                            + publicKey
                            + ", got public key:" + encoded);
        }
    }

    public void checkClientTrusted(X509Certificate[] xcs, String string) {
        // throw new
        // UnsupportedOperationException("checkClientTrusted: Not supported yet.");
    }

    public X509Certificate[] getAcceptedIssuers() {
        // throw new
        // UnsupportedOperationException("getAcceptedIssuers: Not supported yet.");
        return null;
    }
}
