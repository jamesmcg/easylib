/* No License */

package com.jamesmcg.easylib.web;

import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

public class SSLSocketFactoryHelper
{
	public static final String PROTOCOL_SSL = "SSL";

	public static final X509TrustManager trustManagerToAcceptAllCerts = new X509TrustManager()
	{
		public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException
		{
		}

		public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException
		{
		}

		public X509Certificate[] getAcceptedIssuers()
		{
			return null;
		}
	};

	public static SSLSocketFactory newInstanceAcceptAllCerts(String protocol) throws NoSuchAlgorithmException, KeyManagementException
	{
		return newInstance(protocol, trustManagerToAcceptAllCerts);
	}

	public static SSLSocketFactory newInstance(String protocol, TrustManager trustManager) throws NoSuchAlgorithmException, KeyManagementException
	{
		return newInstance(protocol, new TrustManager[] { trustManager });
	}

    /**
     * fix for
     * Exception in thread "main" javax.net.ssl.SSLHandshakeException:
     *     sun.security.validator.ValidatorException:
     *     PKIX path building failed: sun.security.provider.certpath.SunCertPathBuilderException:
     *     unable to find valid certification path to requested target
     * @param protocol parameter for SSLContext.getInstance(protocol). Usually "SSL", can be "TLS"
     */
	public static SSLSocketFactory newInstance(String protocol, TrustManager[] trustManagers) throws NoSuchAlgorithmException, KeyManagementException
	{
		try
		{
			if (protocol == null) throw new NoSuchAlgorithmException("protocol == null");
			
			SSLContext ctx = SSLContext.getInstance(protocol);
			ctx.init(null, trustManagers, new java.security.SecureRandom());
	        return ctx.getSocketFactory();
		}
		catch (NoSuchAlgorithmException e)
		{
			// Protocol is null or not found
			throw e;
		}
        catch (KeyManagementException e)
		{
        	// Initialising the SSLContext failed
			throw e;
		}
	}
}
