/*
Copyright 2014 James E McGuire

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

UriFetcher
*/

package com.jamesmcg.easylib.web;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import android.content.ContentResolver;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;

public class UriFetcher
{
	public interface SaveLocallyCallback
	{
		public void onFinishLoadingFailed(String failMsg);
		public void onFinishLoading(File savedTo);
	}
	
	public interface BitmapCallback
	{
		public void onFinishLoadingFailed(String failMsg, boolean isOutOfMemory);
		public void onFinishLoading(Bitmap bitmap, int angleDegrees);
	}
	
	/** Fetches the given Uri to a local file */
	public static void runFetchToFile(SaveLocallyCallback callback, File saveTo, Uri uri, ContentResolver contentResolver)
	{
		FetchToLocalTask task = new FetchToLocalTask(callback, saveTo, uri, contentResolver);
		task.execute();
	}
	
	public static void runFetchToBitmap(BitmapCallback callback, File file)
	{
		FetchToBitmapTask task = new FetchToBitmapTask(callback, file);
		task.execute();
	}
	
	private static class BitmapResult
	{
		public final Bitmap bitmap;
		public final int angleDegrees;
		
		public BitmapResult(Bitmap bitmap, int angleDegrees)
		{
			this.bitmap = bitmap;
			this.angleDegrees = angleDegrees;
		}
	}
	
	private static class LocalResult
	{
		public final File savedTo;
		
		public LocalResult(File savedTo)
		{
			this.savedTo = savedTo;
		}
	}
	
	private static class TaskResult<X>
	{
		public final boolean success;
		public final String failMsg;
		public final Throwable e;
		public final X successObj;
		
		public TaskResult(Throwable e, String failMsg)
		{
			this.success = false;
			this.failMsg = failMsg;
			this.e = e;
			this.successObj = null;
			
			this.e.printStackTrace();
		}
		
		public TaskResult(X successObj)
		{
			this.success = true;
			this.failMsg = null;
			this.e = null;
			this.successObj = successObj;
		}
	}
	
	private static class FetchToBitmapTask extends AsyncTask<Void, Void, TaskResult<BitmapResult>>
	{
		private BitmapCallback callback;
		private File file;
		
		public FetchToBitmapTask(BitmapCallback callback, File file)
		{
			this.callback = callback;
			this.file = file;
		}
		
		@Override
		protected TaskResult<BitmapResult> doInBackground(Void... arg0)
		{
			TaskResult<BitmapResult> result = null;
			BufferedInputStream bis = null;
			
			outerTry: try
			{
				int angleDegrees;
				try
				{
					if (file == null) angleDegrees = 0;
					else
					{
						ExifInterface exif = new ExifInterface(file.getAbsolutePath());
				        int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);
				        
				        angleDegrees = orientation == ExifInterface.ORIENTATION_NORMAL ? 0
				        		: orientation == ExifInterface.ORIENTATION_ROTATE_90 ? 90
				        		: orientation == ExifInterface.ORIENTATION_ROTATE_180 ? 180
				        		: orientation == ExifInterface.ORIENTATION_ROTATE_270 ? 270
				        		: 0;
					}
				}
				catch (IOException e)
				{
					// Meh, the user can rotate it themselves
					angleDegrees = 0;
				}
		        
				try
				{
					FileInputStream fis = new FileInputStream(file);
					bis = new BufferedInputStream(fis);
				}
				catch (FileNotFoundException e)
				{
					result = new TaskResult<BitmapResult>(e, "The file does not exist");
					break outerTry;
				}
				
				Bitmap bitmap;
				try
				{
					bitmap = BitmapFactory.decodeStream(bis);
				}
				catch (java.lang.OutOfMemoryError e)
				{
					result = new TaskResult<BitmapResult>(e, "Not enough memory to load the entire image :(");
					break outerTry;
				}
				
				if (bitmap == null)
				{
					result = new TaskResult<BitmapResult>(new Exception("bitmap == null"), "The file cannot be used to decode a bitmap");
				}
				else
				{
					result = new TaskResult<BitmapResult>(new BitmapResult(bitmap, angleDegrees));					
				}
			}
			catch (Exception e)
			{
				result = new TaskResult<BitmapResult>(e, "Unknown error: " + e.getMessage());
			}
			finally
			{
				// Try to close is
				if (bis != null)
				{
					try
					{
						bis.close();
					}
					catch (IOException e)
					{
						// This doesn't actually affect the reading operation... its already read everything.
						Log.e(UriFetcher.class.getSimpleName(), "IOException closing bis from file, data read did succeed though");
					}

					bis = null;
				}
			}
			
			return result;
		}
		
		@Override
		protected void onPostExecute(TaskResult<BitmapResult> result)
		{
			if (result.success) callback.onFinishLoading(result.successObj.bitmap, result.successObj.angleDegrees);
			else callback.onFinishLoadingFailed(result.failMsg, result.e instanceof java.lang.OutOfMemoryError);
		}
	}
	
	// We need to use Channels.newChannel to create an InterruptibleChannel, so that we can apply a timeout around the blocking calls to read
	private static class FetchToLocalTask extends AsyncTask<Void, Void, TaskResult<LocalResult>>
	{
		private SaveLocallyCallback callback;
		private File saveTo;
		private Uri uri;
		private ContentResolver contentResolver;
		
		public FetchToLocalTask(SaveLocallyCallback callback, File saveTo, Uri uri, ContentResolver contentResolver)
		{
			this.callback = callback;
			this.saveTo = saveTo;
			this.uri = uri;
			this.contentResolver = contentResolver;
		}
		
		@Override
		protected TaskResult<LocalResult> doInBackground(Void... arg0)
		{
			TaskResult<LocalResult> result = null;
			BufferedOutputStream bos = null;
			InputStream is = null; // We're using a manual buffer of 4192
			
			outerTry: try
			{
				try
				{
					FileOutputStream fos = new FileOutputStream(saveTo);
					bos = new BufferedOutputStream(fos);
				}
				catch (FileNotFoundException e)
				{
					result = new TaskResult<LocalResult>(e, "File cannot be opened for writing");
					break outerTry;
				}
				
				try
				{
					is = contentResolver.openInputStream(uri);
				}
				catch (FileNotFoundException e)
				{
					result = new TaskResult<LocalResult>(e, "The provided URI could not be opened");
					break outerTry;
				}

				byte[] buffer = new byte[4092];
				int bytes = 0;
				do
				{
					try
					{
						bos.write(buffer, 0, bytes);
					}
					catch (IOException e)
					{
						result = new TaskResult<LocalResult>(e, "Write to disk error");
						break outerTry;
					}
					
					try
					{
						bytes = is.read(buffer);
					}
					catch (IOException e)
					{
						result = new TaskResult<LocalResult>(e, "Read from content stream error");
						break outerTry;
					}
				}
				while (bytes >= 0);
				
				result = new TaskResult<LocalResult>(new LocalResult(saveTo));
			}
			catch (Exception e)
			{
				result = new TaskResult<LocalResult>(e, "Unknown error: " + e.getMessage());
			}
			finally
			{
				// Try to close is
				if (is != null)
				{
					try
					{
						is.close();
					}
					catch (IOException e)
					{
						// This doesn't actually affect the reading operation... its already read everything.
						Log.e(UriFetcher.class.getSimpleName(), "IOException closing is from getContentResolver, data read did succeed though");
					}

					is = null;
				}

				// Try to close bos
				if (bos != null)
				{
					try
					{
						bos.close();
					}
					catch (IOException e)
					{
						if (result == null) {} // uncaught "Error", execution won't get past this finally block
						else if (result.success) result = new TaskResult<LocalResult>(e, "Close file on disk error"); // Avoid overwriting a different error
					}

					bos = null;
				}
				
				// Delete the output file if incomplete
				if (result == null || result.success == false)
				{
					// Some error occurred, we will have returned an error obj
					boolean ok = saveTo.delete();
					if (ok == false)
					{
						System.gc(); // last ditch attempt to let bos/is auto close
						ok = saveTo.delete();
					}
					
					if (ok == false)
					{
						Log.e(UriFetcher.class.getSimpleName(), "Fail to delete partially written saveTo file (" + saveTo.getAbsolutePath() + ")");
					}
				}
			}
			
			if (result == null) throw new RuntimeException("Never happen case, this is outside the finally");
			return result;
		}
		
		@Override
		protected void onPostExecute(TaskResult<LocalResult> result)
		{
			if (result.success) callback.onFinishLoading(result.successObj.savedTo);
			else callback.onFinishLoadingFailed(result.failMsg);
		}
	}
}
