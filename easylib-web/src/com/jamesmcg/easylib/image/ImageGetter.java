/*
Copyright 2013 James E McGuire

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

ImageGetter
*/

package com.jamesmcg.easylib.image;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import com.jamesmcg.easylib.web.EasyHttp;
import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.os.Handler;
import android.util.Log;
import android.widget.ImageView;

// Note to self: The small window, after downloads have ended when they are put in the pendingCallbacks queue.
// addUrl won't be able to reuse the existing download in this window.
// Worst case is a new download will be created, but normally the image is still in the memCache.
/**
 * The SD cache directory must be exclusive to the cache; the cache may delete or overwrite files from its directory.
 * It is an error for multiple processes (Multiple ImageGetters) to use the same cache directory at the same time.
 * See documentation here: https://github.com/JakeWharton/DiskLruCache
 * Note: This implementation specifically targets Android compatibility.
 * 
 * A nice threaded image downloader, with two cache levels (mem + sd).
 * It is designed to be compatible with reusable ViewHolder cells in an adapter.
 * If the user scrolls quickly it won't queue up lots of downloads. Only the latest.
 * 
 * note addUrl memCacheType and sdCacheType: if a url is requested and a download is already in progress,
 * its actual memCacheType and sdCacheType will be from the original request
 * 
 * It keeps hard links to bitmaps so make sure to keep its size under control.
 * Hard links are necessary because soft cache using SoftReferences doesn't work
 * properly in the android VM. 3.0+ usually clears these almost instantly. Some
 * phones don't clear these at all, causing the app to run out of memory.
 * 
 * You could perhaps keep one instance of this cache always present in the
 * app's singleton and clear it in Application onLowMemory or Activity onStop for example.
 *
 * Checks for MemCache hit are done on the UI thread, and can return instantly.
 * This class is designed to use multiple background threads:
 * 1 or more for loading images from SD card cache.
 * 1 or more for downloading images from Web. (when not found in SD cache)
 * This means that cached images on the SD card don't have to wait for the next Web download.
 * And that images from web can be fetched from different urls at the same time
 */
public class ImageGetter
{
	/**
	 * Helper class that ensures if two cells request the same url they will both recieve callbacks.
	 * The image will only be downloaded once and up until the last moment additional cells can
	 * hop on the same callback bandwagon. This means there is no need to recheck the cache each
	 * additional download targeting the same url that is already in progress.
	 */
	private static class Download
	{
		private enum State
		{
			WAITING_SD,
			WAITING_WEB,
			DOWNLOADING_SD,
			DOWNLOADING_SD_CANCEL_WHEN_COMPLETE,
			DOWNLOADING_WEB,
			DOWNLOADING_WEB_CANCEL_WHEN_COMPLETE,
			// This state means the download is complete and a PendingCallback object has been added for each callback
			// This state means the only references to Download are kept in pendingCallbacks
			ENDED_OBJ_ONLY_IN_PENDING_CALLBACKS
		}

		// Callbacks are removed from this list if a request comes in since this was enqueued.
		// That way they won't be notified of a download they no longer care about.
		public final LinkedList<Callback> interestedParties = new LinkedList<Callback>();
		public final String url;
		public final SDCacheType sdCacheType;
		public final MemCacheType memCacheType;
		
		public State state;
		
		/** Null indicates error, if State == COMPLETE */
		public Bitmap complete = null;
		
		/**
		 * Creates a new download object with interestedParties.size() == 0
		 * @param url - The url of this download, it can never change
		 * @param state - How far along the process queue this download is, this changes as appropriate
		 * @param sdCacheType - Whether the image needs to preserve alpha channel when written to SD card
		 */
		public Download(String url, State state, SDCacheType sdCacheType, MemCacheType memCacheType)
		{
			this.url = url;
			this.state = state;
			this.sdCacheType = sdCacheType;
			this.memCacheType = memCacheType;
		}
	}
	
	/**
	 * This is necessary to allow the callbacks to occur, without any side effects.
	 * This allows addUrl to be called from inside a callback without any side effects.
	 * This allows callbacks to occur without them being out of date.
	 */
	private static class PendingCallback
	{
		public enum Type
		{
			// NOT_IN_MEM_CACHE,
			// NOT_IN_MEM_CACHE_AND_NOT_IN_SD_CACHE,
			NOT_IN_SD_CACHE,
			RESULT_FAIL,
			RESULT_SUCCESS_SD,
			RESULT_SUCCESS_WEB
			// COMPLETE
		}
		
		public final Callback callback;
		public final Type type;
		public final Download download;
		
		public PendingCallback(Callback callback, Type type, Download download)
		{
			if (callback == null) throw new IllegalArgumentException("callback == null");
			if (type == null) throw new IllegalArgumentException("type == null");
			if (download == null) throw new IllegalArgumentException("download == null");
			
			this.callback = callback;
			this.type = type;
			this.download = download; // Used to get quick access to download.bitmap for the success callback
		}
	}
	
	public static enum MemCacheType
	{
		ENABLED,
		DISABLED
	}
	
	/**
	 * This is used to alter the sd cache filename of 2 identical urls saved with different bitmap settings.
	 * Unexpected cache issues could result otherwise
	 */
	public static class SDCacheType
	{
		public static SDCacheType PNG_WITH_ALPHA_FULL = new SDCacheType(Bitmap.CompressFormat.PNG, -1);
		public static SDCacheType JPEG_NO_ALPHA_FULL = new SDCacheType(Bitmap.CompressFormat.JPEG, 100);
		public static SDCacheType JPEG_NO_ALPHA_HIGH = new SDCacheType(Bitmap.CompressFormat.JPEG, 85);
		public static SDCacheType JPEG_NO_ALPHA_MEDIUM = new SDCacheType(Bitmap.CompressFormat.JPEG, 60);
		public static SDCacheType JPEG_NO_ALPHA_LOW = new SDCacheType(Bitmap.CompressFormat.JPEG, 45);
		public static SDCacheType DISABLED = new SDCacheType(Bitmap.CompressFormat.JPEG, 100); // "Special" object - format and quality are ignored
		
		/** must conform to [a-z0-9_] */
		private final String sdCache3LetterCode;
		
		public final Bitmap.CompressFormat compressFormat;
		public final int compressQuality;
		
		/**
		 * @param compressFormat - compress format for the bitmap, ideally JPG or PNG
		 * @param compressQuality - 0 to 100 compress quality for the bitmap, ignored for PNG
		 */
		@SuppressLint("NewApi")
		public SDCacheType(Bitmap.CompressFormat compressFormat, int compressQuality)
		{
			// PNG is always uncompressed (bitmap.compress ignores quality setting), so make it always cache to the same file
	    	if (compressFormat == CompressFormat.PNG) compressQuality = 100;
	    	
			if (compressFormat == null) throw new IllegalArgumentException("compressFormat == null");
	    	if (compressQuality < 0) throw new IllegalArgumentException("compressQuality < 0");
	    	if (compressQuality > 100) throw new IllegalArgumentException("compressQuality > 100");
			
			// Use this string because the CompressFormat ordinal could change if the order of enum elements is changed.
			final String compressFormat1Letter
					= compressFormat == CompressFormat.JPEG ? "0"
					: compressFormat == CompressFormat.PNG ? "1"
					: compressFormat == CompressFormat.WEBP ? "2" : "";
			
			this.sdCache3LetterCode = compressFormat1Letter + (compressQuality < 16 ? "0" : "") + Integer.toHexString(compressQuality);
			this.compressFormat = compressFormat;
			this.compressQuality = compressQuality;
			
			if (sdCache3LetterCode.length() != 3) throw new IllegalArgumentException("sdCache3LetterCode.length() != 3");
		}
	}
	
	/**
	 * Grabs the requested image from web. Return null on error.
	 * @param context - Required to check network accessibility and give a nice error if unavailable
	 */
	private static Bitmap loadBitmapFromUrl(EasyHttp.Config imageFetchConfig, String url)
	{
		EasyHttp http = new EasyHttp(imageFetchConfig);
		EasyHttp.Result<Bitmap> result = http.runWantBitmap(url);
		
		return result.success ? result.data : null;
	}

	/** Returns null if unavailable, this is very unusual on android */
	private static MessageDigest getMD5Digester()
	{
		try
		{
			return MessageDigest.getInstance("MD5");
		}
		catch (NoSuchAlgorithmException e)
		{
			Log.e(ImageGetter.class.getSimpleName(), "NoSuchAlgorithm: MD5");
			return null;
		}
	}
	
	/**
	 * YOU MUST USE A DIFFERENT INSTANCE OF MD5 DIGESTER PER THREAD.
	 * Returns the lowercase hex MD5 of the url, or null if MD5 is not available.
	 * This is used because keys for jakewharton disk cache must match this regex [a-z0-9_]{1,64}
	 */
	private static String urlToMD5FileName(MessageDigest md5Digester, String url, SDCacheType sdCacheType)
	{
		// produces lower case 32 char wide hexa left-padded with 0
		return sdCacheType.sdCache3LetterCode + String.format("%032x", new BigInteger(1, md5Digester.digest(url.getBytes())));
	}

    private enum SDState
    {
    	OPEN,
    	CLOSED,
    	CLOSED_FOREVER,
    	CLOSED_PENDING_RETRY
    }
    
	/**
	 * Each request attempts in order - memory cache, sd cache, download from web (with an early exit if available)
	 * Only the last requested URL is kept for any one callback instance. If the requested URL is changed
	 * for a callback instance, no more callbacks for the "old" URL will occur.
	 * Requests for the same URL will continue to download from SD or web if already running.
	 */
	public interface Callback
	{
		public enum LoadSource
		{
			MEM_CACHE_INSTANT,
			SD_CACHE,
			WEB
		}
		
		/**
		 * Guaranteed not to be called after any onFinishX method.
		 * 
		 * Called after a memCache miss, indicating a SD or web fetch will run.
		 * The requested url will have to wait for SD card as well as a potential web download.
		 */
		public void onNotInMemCache();
		
		/**
		 * Guaranteed not to be called after any onFinishX method.
		 * This call might not happen at all (if its not done by the time an onFinishX call is queued for UI thread).
		 * 
		 * Called when added to the queue for a web fetch. This indicates that it will be
		 * a while until the requested image is available. (Display a loading icon or spinner)
		 */
		public void onNotInSDCache();
		
		/**
		 * onNotInSDCache method might not be called before this.
		 * 
		 * The last requested image for this callback was fetched successfully.
		 * You can now stop any spinners etc. that started in onNotInMemCache or onNotInSDCache.
		 * @param image - Not null. The requested image. 
		 */
		public void onFinishLoading(Bitmap image, String url, LoadSource source);
		
		/**
		 * onNotInSDCache method might not be called before this.
		 * 
		 * Called when the fetch from web failed, or the requested URL was null.
		 * You can now stop any spinners etc. that started in onNotInMemCache or onNotInSDCache.
		 */
		public void onFinishLoadingFailed(String url);
	}
	
	private static class AutomaticCallback implements Callback
	{
		private final ImageView imageView;
		private int failedResourceId;
		private boolean forUseInAdapter;
		
		public AutomaticCallback(ImageView imageView, int failedResourceId, boolean forUseInAdapter)
		{
			this.imageView = imageView;
			this.failedResourceId = failedResourceId;
			this.forUseInAdapter = forUseInAdapter;
		}
		
		public void setFailedResourceId(int failedResourceId)
		{
			this.failedResourceId = failedResourceId;
		}

		@Override
		public void onNotInMemCache()
		{
			imageView.setImageDrawable(null);
		}

		@Override
		public void onNotInSDCache()
		{
		}

		@SuppressLint("NewApi")
		@Override
		public void onFinishLoading(Bitmap image, String url, LoadSource source)
		{
			if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.HONEYCOMB_MR1)
			{
				// N.B. If the image was available immediately from mem cache, do not fade in
				
				// There is a bug in ExpandableListView when on collapsing a list it creates new rows (convertView == null)
				// which would otherwise trigger the fade in effect, which just looks weird
				
				// https://code.google.com/p/android/issues/detail?id=38581
				// See this bug in both ListView and ExpandableListView where the animation never starts sometimes,
				// presumably when Android thinks a layout pass has been done or something. At any rate the image doesn't
				// appear until the user touches the screen (that's when it refreshes the list and the animation runs)
				if (imageView.getDrawable() != null || (forUseInAdapter && source == LoadSource.MEM_CACHE_INSTANT))
				{
					// Image is already present (i.e. view reused and imageView.setImageDrawable(null) was not called), just change it straight away without fade
					// or the image was available immediately from mem cache
					imageView.setImageBitmap(image);
				}
				else
				{
					imageView.setAlpha(0f);
					imageView.setImageBitmap(image);
					
					// N.B. putting this animate call in a handler can fix issue 38581 of the animation not starting but it can
					// trigger a continually fading in loop on ExpandableListView when it creates a new convertView/AutomaticCallback
					// every screen refresh, which queues an animation (therefore another refresh) each time
					imageView.animate().alpha(1).setDuration(210); // N.B. start() is api 14
				}
			}
			else imageView.setImageBitmap(image);
		}

		@Override
		public void onFinishLoadingFailed(String url)
		{
			imageView.setImageResource(failedResourceId);
		}
	}
	
	/**
	 * This method will return you a Callback, reusing the existing one if imageView.getTag() is set,
	 * or creating a new one and assigning it to the tag if imageView.getTag() is unset
	 * 
	 * If you want the image to fade in when instantly available from MemCache (*and this is not being used
	 * within an adapter*) call convenienceGetCallbackByTag(ImageView, int, boolean) with forUseInAdapter = false
	 */
	public static Callback convenienceGetCallbackByTag(ImageView imageView, int failedResourceId)
	{
		return convenienceGetCallbackByTag(imageView, failedResourceId, true);
	}
	
	/**
	 * This method will return you a Callback, reusing the existing one if imageView.getTag() is set,
	 * or creating a new one and assigning it to the tag if imageView.getTag() is unset
	 * @param imageView The imageView which will be displaying the image
	 * @param failedResourceId Drawable resource to apply if the image fails to load
	 * @param forUseInAdapter ALWAYS set to true if you are using this for a ListView or ExpandableListView adapter.
	 *                                            Set to false if you want the image to fade in when instantly available from MemCache
	 *                                            (True applies a workaround for an Android bug affecting the fade in animation.)
	 */
	public static Callback convenienceGetCallbackByTag(ImageView imageView, int failedResourceId, boolean forUseInAdapter)
	{
		Object tag = imageView.getTag();
		if (tag instanceof AutomaticCallback)
		{
			AutomaticCallback callback = (AutomaticCallback) tag;
			callback.setFailedResourceId(failedResourceId);
			return callback;
		}
		else
		{ 
			Callback callback = new AutomaticCallback(imageView, failedResourceId, forUseInAdapter);
			imageView.setTag(callback);
			return callback;
		}
	}
	
	// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

	private final int WRITE_BITMAP_TO_SD_STREAM_BUFFER_BYTES;

	private final File DISC_CACHE_DIR_FILE;
	private final int DISC_CACHE_APP_VERSION;
	private final int DISC_CACHE_VALUE_COUNT;
	private final int DISC_CACHE_MAX_SIZE_BYTES;
	
	// If the cache fails to be initialised or fails to close, the app won't try again until after x skipped attempts
	private final int IGNORE_COUNT_BEFORE_RETRYING_SD_CACHE;

    /**
     * Do not set either of these to 0, the rest of the code can't handle it yet
     * SD - Ideally 2 or 3, rarely more are even started up, let alone utilised.
     * WEB  - Ideally 4 or 5, a large number here might slow down/lag ui on older phones but will download faster
     */
    private final int MAX_SD_WORKERS = 3;
    private final int MAX_WEB_WORKERS = 5;
    
    // One cell can only request one download, this keeps track of their latest request.
    // Worker threads pick elements from this list to process.
    // Process order is unspecified, they just grab the first HashMap entry.
    private final HashMap<Callback, Download> downloadByCell = new HashMap<Callback, Download>();
    
    // Contains all non-completed downloads. 
    // This may have more elements than downloadByCell if interestedParties.size() == 0 on some of the downloads.
    // This may have fewer elements than downloadByCell if interestedParties.size() > 1 on some of the downloads.
    private final HashMap<String, Download> downloadByUrl = new HashMap<String, Download>();
    
    // This contains requests that are in progress, but need to give callbacks:
    // It also holds the result of a completed (or failed) download, for the necessary callback
    private final HashMap<Callback, PendingCallback> pendingCallbacks = new HashMap<Callback, PendingCallback>();

    // Note: jakewharton SD cache is thread safe, but the other cache variables need synchronising
    // It can use a different sync object to the hashmaps, this is that object.
    private final Object sdCacheSync = new Object();
    
    private final boolean debugThreads;
    
    private final EasyHttp.Config easyHttpConfig;
    
    // Handler performs SD and Web callbacks on the UI thread
    private final Handler handler;
	
    // Hard cache which will attempt to use up to a certain percent of "free heap memory"
    private final BitmapLruCache memCache;

	// SD cache is initialised on SD thread the first time a download is grabbed as it requires file ops.
    private SDState sdCacheState;
	private com.jakewharton.disklrucache.DiskLruCache sdCache = null;
	private int sdCacheIgnoreCount = 0;
	
	// These lists become empty when the threads have stopped.
	private final HashSet<Thread> workersSD = new HashSet<Thread>();
	private final HashSet<Thread> workersWeb = new HashSet<Thread>();
    //private Thread workerSD = null;
    //private Thread workerWeb = null;
	
    /**
     * This constructor MUST be instantiated on the UI thread as it creates a handler.
     * The handler performs most of the callbacks for SD and Web.
     * Some cache callbacks are performed immediately from addUrl(Callback,String).
     * 
     * @param memCacheUseFraction - (0.01 <= x <= 0.99)
     *        Fraction of free heap space to use for memory cache.
     *        0.2 is a reasonable value. This will dynamically change every time an item is
     *        added to the memory cache (or refreshed by loading from SD or Web).
     * @param discCacheDir - (!= null) File to use the disc cache with.
     *        It is an error for more than one ImageGetter to be initialised with the same sd cache folder.
     *        Note an ImageGette continues running on its sd cache folder until the last sd thread terminates.
     * @param discCacheAppVersion - (1 <= x) Increment to ignore cache files created with older versions.
     * @param discCacheLimitBytes - (1 <= x) The limit in bytes for the disc cache.
     * @param imageTimeoutConnection - (0 < x) Compulsory timeout in ms, to send data to server and for it to acknowledge connection initiated
     * @param imageTimeoutRead - (0 < x) Compulsory timeout in ms, for server to generate and send response and for it to arrive
     */
    public ImageGetter(
    		boolean debugMemCache,
    		boolean debugThreads,
    		double memCacheUseFraction,
    		File discCacheDir,
    		int discCacheAppVersion,
    		int discCacheLimitBytes,
    		EasyHttp.Config imageFetchConfig)
    {
    	if (memCacheUseFraction < 0.01)  throw new IllegalArgumentException("memCacheUseFraction < 0.01");
    	if (memCacheUseFraction > 0.99)  throw new IllegalArgumentException("memCacheUseFraction > 0.99");
    	if (discCacheDir == null) 		 throw new IllegalArgumentException("discCacheDir == null");
    	if (discCacheAppVersion < 1) 	 throw new IllegalArgumentException("discCacheAppVersion < 1");
    	if (discCacheLimitBytes < 1) 	 throw new IllegalArgumentException("discCacheLimitBytes < 1");
    	
    	this.WRITE_BITMAP_TO_SD_STREAM_BUFFER_BYTES = 4092;

    	this.DISC_CACHE_DIR_FILE = discCacheDir;
    	this.DISC_CACHE_APP_VERSION = discCacheAppVersion;
    	this.DISC_CACHE_VALUE_COUNT = 1;
    	this.DISC_CACHE_MAX_SIZE_BYTES = discCacheLimitBytes;
    	
    	this.IGNORE_COUNT_BEFORE_RETRYING_SD_CACHE = 20;

    	this.debugThreads = debugThreads;
    	this.easyHttpConfig = imageFetchConfig;
    	this.handler = new Handler();
    	this.memCache = new BitmapLruCache(debugMemCache, memCacheUseFraction);
    	this.sdCacheState = SDState.CLOSED;
    }
    
    /**
     * ImageGetter without the SD cache.
     * This constructor MUST be instantiated on the UI thread as it creates a handler.
     * The handler performs most of the callbacks for SD and Web.
     * Some cache callbacks are performed immediately from addUrl(Callback,String).
     * 
     * @param memCacheUseFraction - (0.01 <= x <= 0.99)
     *        Fraction of free heap space to use for memory cache.
     *        0.2 is a reasonable value. This will dynamically change every time an item is
     *        added to the memory cache (or refreshed by loading from SD or Web).
     * @param imageFetchConfig - Config for fetching images, including timeouts
     */
    public ImageGetter(
    		boolean debugMemCache,
    		boolean debugThreads,
    		double memCacheUseFraction,
    		EasyHttp.Config imageFetchConfig)
    {
    	if (memCacheUseFraction < 0.01)  throw new IllegalArgumentException("memCacheUseFraction < 0.01");
    	if (memCacheUseFraction > 0.99)  throw new IllegalArgumentException("memCacheUseFraction > 0.99");
    	
    	this.WRITE_BITMAP_TO_SD_STREAM_BUFFER_BYTES = -1;

    	this.DISC_CACHE_DIR_FILE = null;
    	this.DISC_CACHE_APP_VERSION = -1;
    	this.DISC_CACHE_VALUE_COUNT = -1;
    	this.DISC_CACHE_MAX_SIZE_BYTES = -1;
    	
    	this.IGNORE_COUNT_BEFORE_RETRYING_SD_CACHE = 20;

    	this.debugThreads = debugThreads;
    	this.easyHttpConfig = imageFetchConfig;
    	this.handler = new Handler();
    	this.memCache = new BitmapLruCache(debugMemCache, memCacheUseFraction);
    	this.sdCacheState = SDState.CLOSED_FOREVER;
    }
    
    /**
     * Removes cell entry from downloadByCell if present,
     * Removes cell from a existing download.interestedParties if present
     * Removes cell from pendingCallbacks if present
     * calls bitmap.recycle() on an unused download if this cell will be the last one to see it
     */
    private void removeCellReferenceIfExistsNeedsSync(Callback cell)
    {
		// As this request is newer, cancel any downloads pending or remove callbacks for any in progress.
		Download queued = downloadByCell.remove(cell);
		if (queued != null)
		{
			// If interestedParties size == 0 now, it doesn't matter.
			// Background thread will remove the download (from downloadByUrl) when its done, if queued.interestedParties.size() == 0
			queued.interestedParties.remove(cell);
		}
		
		// Cancel any completed download callbacks, or other callbacks yet to be fired
		/*PendingCallback pending =*/ pendingCallbacks.remove(cell);
		
		//!! IllegalStateException Can't compress a recycled bitmap (thread is still writing it to sd card by the time this kicks in)
		/*// To help the garbage collector we can recycle some memory early
		if (pending != null && pending.download.complete != null && pending.download.interestedParties.size() == 0)
		{
			// Complete downloads are only ever kept in pendingCallbacks, there aren't any more callbacks for this one
			pending.download.complete.recycle();
			pending.download.complete = null;
		}*/
    }
    
    /** Starts another SD thread if the maximum has yet to be reached */
    private void startAnotherSDWorkerNeedsSync()
    {
    	if (workersSD.size() < MAX_SD_WORKERS)
    	{
    		Thread t = new Thread(workerSDRunnable);
    		workersSD.add(t);
    		t.start();
    	}
    }
    
    private void startAnotherWebWorkerNeedsSync()
    {
    	if (workersWeb.size() < MAX_WEB_WORKERS)
    	{
    		Thread t = new Thread(workerWebRunnable);
    		workersWeb.add(t);
    		t.start();
    	}
    }

    /**
     * Note: onFinishLoadingFailed will NOT be called. Neither will any other callback methods.
     * You can call it yourself if you like once this method returns
     * @param callback - cannot be null
     */
    public void cancelExistingCallback(Callback callback)
    {
    	if (callback == null) throw new IllegalArgumentException("callback == null");
    	
		synchronized (ImageGetter.this)
        {
			removeCellReferenceIfExistsNeedsSync(callback);
        }
    }
    
    /**
     * Be aware that SDCacheType and MemCacheType will be ignored if the requested url is already downloading.
     * The initial request for that url will define the SDCacheType and MemCacheType.
     * 
     * Its safe to call addUrl (with the same or a different url) from inside any of the callbacks.
	 * This method triggers the following callbacks instantly. (Called from here, on the same thread)
	 * onFinishLoadingFailed() - if url is null.
     * onFinishLoading(image) - if the file is in memory cache.
     */
    public void addUrl(Callback callback, SDCacheType sdCacheType, String url)
    {
    	addUrl(callback, sdCacheType, MemCacheType.ENABLED, url);
    }
    
    /**
     * Be aware that SDCacheType and MemCacheType will be ignored if the requested url is already downloading.
     * The initial request for that url will define the SDCacheType and MemCacheType.
     * 
     * Its safe to call addUrl (with the same or a different url) from inside any of the callbacks.
	 * This method triggers the following callbacks instantly. (Called from here, on the same thread)
	 * onFinishLoadingFailed() - if url is null.
     * onFinishLoading(image) - if the file is in memory cache.
     */
    public void addUrl(Callback callback, SDCacheType sdCacheType, MemCacheType memCacheType, String url)
    {
    	if (callback == null) throw new IllegalArgumentException("callback == null");
    	if (sdCacheType == null) throw new IllegalArgumentException("sdCacheType == null");
    	if (memCacheType == null) throw new IllegalArgumentException("memCacheType == null");

    	Bitmap image;

		// It's ok for any callback to occur outside a synch block and within this method.
		// Its only ok if no more "work" or client callbacks occur in this method after the first callback.
		// N.B. otherwise reentrant error will occur if the first client callback calls addUrl again
		// (or any other method that edits the state of downloads or pending callbacks)
    	
		// What are we meant to do with this?
    	if (url == null || url.length() == 0)
    	{
    		synchronized (ImageGetter.this)
            {
    			removeCellReferenceIfExistsNeedsSync(callback);
            }
    		callback.onFinishLoadingFailed(url);
    	}
    	// Check mem cache on UI thread, to avoid needless churn
    	// Mem cache (android LruCache) is thread safe, just call it
    	else if (memCacheType == MemCacheType.ENABLED && (image = memCache.get(url)) != null)
    	{
			// Cache hit.
    		synchronized (ImageGetter.this)
            {
    			removeCellReferenceIfExistsNeedsSync(callback);
            }
    		callback.onFinishLoading(image, url, Callback.LoadSource.MEM_CACHE_INSTANT);
    	}
    	// We need to get this from SD or Web
    	else
    	{
			// Pending callback to add, in addition to the notInMemory callback. Make sure to add this
    		// BEFORE performing the notInMemory callback, otherwise the client could call addUrl again,
    		// in which case it would expect to be able to edit the pending callback queue.
			final PendingCallback.Type additionalPendingCallback;
			
    		synchronized (ImageGetter.this)
            {
    			// If calling addUrl with the same cell and same URL, the image should always be in memCache,
    			// So this point will not normally be reached. On the off chance it is just fetch it again
    			
    			// This will remove pendingCallback entry, downloadByCell entry, interestedParties entry
    			removeCellReferenceIfExistsNeedsSync(callback);
			
    			// This handles the potential situation where new_url == old_url, without interrupting download progress
    			Download dlRequest = downloadByUrl.get(url);
    			if (dlRequest == null)
    			{
    				// Enqueue a new download
    				dlRequest = new Download(url, Download.State.WAITING_SD, sdCacheType, memCacheType);
    				downloadByUrl.put(url, dlRequest);
    			}
    			
    			// Right no there is no existing reference to this cell anywhere, add it to the download we want
    			downloadByCell.put(callback, dlRequest);
    			dlRequest.interestedParties.add(callback);
    			
    			// If a download already exists, bring this cell up to speed with latest progress callback(s).
    			switch (dlRequest.state)
    			{
    				case WAITING_SD:
    					// We just created this object, or it hasn't started yet
    					additionalPendingCallback = null;
    					startAnotherSDWorkerNeedsSync();
    					break;
    				case WAITING_WEB:
    					additionalPendingCallback = PendingCallback.Type.NOT_IN_SD_CACHE;
    					break;
    				case DOWNLOADING_SD:
    					// Download was found (or potentially about to be found) in SD cache
    					// a result will be available pretty soon (as long as its read ok)
    					additionalPendingCallback = null;
    					break;
    				case DOWNLOADING_SD_CANCEL_WHEN_COMPLETE:
    					// Download was found (or potentially about to be found) in SD cache
    					// a result will be available pretty soon (as long as its read ok)
    					additionalPendingCallback = null;
    					// We now care about the result of this download, uncancel.
    					dlRequest.state = Download.State.DOWNLOADING_SD;
    					break;
    				case DOWNLOADING_WEB:
    					// Image is currently downloading from web
    					additionalPendingCallback = PendingCallback.Type.NOT_IN_SD_CACHE;
    					break;
    				case DOWNLOADING_WEB_CANCEL_WHEN_COMPLETE:
    					// Image is currently downloading from web
    					additionalPendingCallback = PendingCallback.Type.NOT_IN_SD_CACHE;
    					// We now care about the result of this download, uncancel.
    					dlRequest.state = Download.State.DOWNLOADING_WEB;
    					break;
    				case ENDED_OBJ_ONLY_IN_PENDING_CALLBACKS:
    					// We would need to manually add the pending complete callback, if downloads
    					// were kept in downloadByUrl, as we "missed the boat" on the bulk adding
    					throw new RuntimeException("Never happen case, completed downloads are removed from downloadByUrl");
    				default:
    					throw new RuntimeException("Never happen case");
    			}

				// Because work would occur in this method, after this callback, we must use the delayed callback
	    		// (To avoid deadlock and out of step calls if the callback calls addUrl again with a different url)
    			if (additionalPendingCallback != null)
    			{
    				pendingCallbacks.put(callback, new PendingCallback(callback, additionalPendingCallback, dlRequest));
    			}
            }
    		
    		// The handler doesn't need to be synchronized, it just needs to be some point
    		// after pendingCallbacks are added. Worst case 2 runnables are enqueued
    		if (additionalPendingCallback != null)
    		{
				handler.removeCallbacks(completedCallbacksRunnable);
	    		handler.post(completedCallbacksRunnable);
    		}
    		
    		callback.onNotInMemCache();
    	}
    }

    /** When you call this method no cells will receive any further callbacks, all callbacks are cancelled (until addUrl is called again) */
    public void cancelAllWithoutCallbacks()
    {
    	synchronized (ImageGetter.this)
    	{
    		downloadByCell.clear();

			//!! Can't recycle because of delayed sd write outside of synch block. Write might still be in progress
    		/*// Process completed downloads, that are no longer needed
    		for (PendingCallback p : pendingCallbacks.values())
    		{
    			// Either all PendingCallbacks for a given download will have been fired off, or none will.
    			// We can safely recycle every bitmap in pendingCallbacks, they are not referenced from anywhere else until the callbacks are fired.
    			if (p.download.complete != null)
    			{
    				p.download.complete.recycle();
    				p.download.complete = null;
    			}
    		}*/
    		pendingCallbacks.clear();
    		
    		
    		// Clear interested parties on all downloads (we just removed from downloadByCell)
    		// Don't delete the downloads as they may be in progress, or pending completion callback
    		for (Download download : downloadByUrl.values())
    		{
    			// Background thread will remove any entries with interestedParties.size() == 0, if it hasn't started
    			download.interestedParties.clear();
    			
    			// If it has started, we need to tell the thread that we don't want it to cache any image it downloads,
    			// And that nothing more should be done
    			if (download.state == Download.State.DOWNLOADING_SD)
    			{
    				download.state = Download.State.DOWNLOADING_SD_CANCEL_WHEN_COMPLETE;
    			}
    			else if (download.state == Download.State.DOWNLOADING_WEB)
    			{
    				download.state = Download.State.DOWNLOADING_WEB_CANCEL_WHEN_COMPLETE;
    			}
    		}

    		// In progress downloads are left in downloadByUrl, until one of the threads removes them (they now have no interestedParties)
    		// Completed downloads will not have any references any more. That's why we recycled the images above
    	}
    }
    
    /**
     * Empties the mem cache, not affecting any existing downloads (which will be added to the cache again on completion).
     * Call clearMemCacheAndCancel to avoid adding these
     */
    public void clearMemCache()
    {
    	// Android LruCache is thread safe
    	memCache.clear();
    }
    
    /** Identical to calling cancelAll() followed by clearMemCache(), and not the other way around */
    public void clearMemCacheAndCancelWithoutCallbacks()
    {
    	cancelAllWithoutCallbacks();
    	clearMemCache();
    }
    
	/**
	 * Grabs the requested image from SD card cache. Return null on error or if the file is not cached.
	 * @param md5Digester - (!= null) MessageDigest to use, knowing that it is not a thread safe class. One per thread please.
	 * @param url - (!= null) Url key to search sd cache for
	 */
	private Bitmap loadBitmapFromSDSnapshotSynched(MessageDigest md5Digester, String url, SDCacheType sdCacheType)
	{
		// Initialise the cache for the very first time
		// We don't need to sync the editor itself, as this is always non-null until the very last thread closes (both SD and Web have to be closed)
		synchronized (sdCacheSync)
		{
			switch (sdCacheState)
			{
				case OPEN:
					// Continue to use it
					break;
				case CLOSED_FOREVER:
					// Never mind, we don't want to use a sd cache
					return null;
				case CLOSED:
					// Try to open it
					try
					{
						sdCache = com.jakewharton.disklrucache.DiskLruCache.open(DISC_CACHE_DIR_FILE, DISC_CACHE_APP_VERSION, DISC_CACHE_VALUE_COUNT, DISC_CACHE_MAX_SIZE_BYTES);
						sdCacheState = SDState.OPEN;
						break;
					}
					catch (IOException e)
					{
			            e.printStackTrace();
			            
						// thrown if reading or writing the cache directory fails
			            sdCacheState = SDState.CLOSED_PENDING_RETRY;
						return null;
					}
				case CLOSED_PENDING_RETRY:
					// Set state to CLOSED, if ignore count is high enough, so the cache gets used next time we need it
					if (sdCacheIgnoreCount >= IGNORE_COUNT_BEFORE_RETRYING_SD_CACHE)
					{
						sdCacheState = SDState.CLOSED;
						sdCacheIgnoreCount = 0;
						return null;
					}
					else
					{
						sdCacheIgnoreCount++;
						return null;
					}
				default:
					throw new RuntimeException("Never happen case");
			}
		}
		
		// Convert the url to md5 hex key because of allowed character limitation [a-z0-9_]{1,64} (by the time a collision happens I'll probably be dead).
		String fileNameKey = urlToMD5FileName(md5Digester, url, sdCacheType);
		if (fileNameKey == null) return null;
		
		// Load the bitmap from sd card
		com.jakewharton.disklrucache.DiskLruCache.Snapshot snapshot = null;
        try
        {
            snapshot = sdCache.get(fileNameKey);
            if (snapshot == null)
            {
            	// File not found in cache
                return null;
            }
            
            // File found in cache - load a bitmap from it
            InputStream in = snapshot.getInputStream(0);

        	// Return null on decode error
            return in == null ? null : BitmapFactory.decodeStream(in);
        }
        catch (IOException e)
        {
            e.printStackTrace();
            return null;
        }
        catch (java.lang.OutOfMemoryError e)
        {
        	Log.e("ImageGetter", "java.lang.OutOfMemoryError on SD decode");
        	e.printStackTrace();
        	return null;
        }
        finally
        {
            if (snapshot != null)
            {
            	// This won't throw IOException.
            	// Also closes the input stream returned by snapshot.getInputStream(0); 
            	snapshot.close();
            }
        }
	}

	/**
	 * Performs the actual write to cache.
	 * True does NOT guarantee the file was committed ok to the disc cache
	 * Closes the output stream, does NOT close the editor
	 */
    private boolean writeBitmapToFile(Bitmap bitmap, SDCacheType sdCacheType, com.jakewharton.disklrucache.DiskLruCache.Editor editor)
    		throws IOException
    {
        OutputStream out = null;
        try
        {
            out = new BufferedOutputStream(editor.newOutputStream(0), WRITE_BITMAP_TO_SD_STREAM_BUFFER_BYTES);
            
            // N.B. on FileNotFoundException the library gives us an OutputStream that
            // silently eats all writes and does nothing. This will return true
            // thinking that everything went OK.
            return bitmap.compress(sdCacheType.compressFormat, sdCacheType.compressQuality, out);
        }
        finally
        {
            if (out != null)
            {
            	out.close();
            }
        }
    }
    
    /**
     * Attempts to write the given bitmap to the disc cache. Fails silently.
	 * @param md5Digester - (!= null) MessageDigest to use, knowing that it is not a thread safe class. One per thread please.
	 * @param url - (!= null) Url key to search sd cache for
	 * @param bitmap - (!= null) Bitmap to write to the SD cache
     */
	private void writeBitmapToSDCacheSynched(MessageDigest md5Digester, String url, Bitmap bitmap, SDCacheType sdCacheType)
	{
		// We don't need to sync the editor itself, as this is always non-null until the very last thread closes (both SD and Web have to be closed)
		synchronized (sdCacheSync)
		{
			if (sdCacheState != SDState.OPEN)
			{
				return;
			}
		}
		
		// Legal keys much match this regex: [a-z0-9_]{1,64}
		String fileNameKey = urlToMD5FileName(md5Digester, url, sdCacheType);
		if (fileNameKey == null) return;
		
		com.jakewharton.disklrucache.DiskLruCache.Editor editor = null;
        try
        {
        	// An entry may have only one editor at one time; if a value is not available to be edited then edit will return null.
            editor = sdCache.edit(fileNameKey);
            if (editor == null)
            {
            	Log.e("ImageGetter", "SD cache editor null for: " + fileNameKey);
                return;
            }

            if (writeBitmapToFile(bitmap, sdCacheType, editor))
            {               
            	sdCache.flush();
                editor.commit();
            }
            else
            {
                editor.abort();
            }
        }
        catch (IOException e)
        {
            try
            {
                if (editor != null)
                {
                    editor.abort();
                }
            }
            catch (IOException ignored) { }
        }
	}
	
	/**
	 * Must be called inside a SYNCHRONIZED block.
	 * Fetches the next Download (no order) from fetchLastRequestByUrl.
	 * Deletes and ignores waiting downloads with interestedParties.size() == 0 as it finds them
	 * Guaranteed no downloads left with interestedParties.size() == 0 when this method has returned null. SD or WEB type doesn't matter
	 * Returns null if there aren't any left to process.
	 * Returns result where download.state WAITING_... matches SD or WEB based on inSDQueueNotWeb.
	 * Returns result which is guaranteed to have interestedParties.size() > 0
	 */
	private Download nextDownloadNoSyncAndDeleteUnwanted(boolean inSDQueueNotWeb)
	{
		Iterator<Download> iterator = downloadByUrl.values().iterator();
		while (iterator.hasNext())
		{
			Download download = iterator.next();
			
			// This method is called from the background threads, when they don't have a current download to process.
			if (download.state == Download.State.ENDED_OBJ_ONLY_IN_PENDING_CALLBACKS)
			{
				throw new RuntimeException("Never happen case, completed downloads should be removed from downloadByUrl");
			}
			
			if (download.interestedParties.size() == 0)
			{
				// Orphaned download, we can delete it as long as its not being processed
				if (download.state == Download.State.WAITING_SD || download.state == Download.State.WAITING_WEB)
				{
					// Item won't be in downloadByCell because there are no interested cells
					iterator.remove(); // Remove from downloadByUrl
				}
				else
				{
					// Leave it for now, its currently downloading, interestedParties.size() will be checked when downloading is complete
				}
			}
			else if (download.state == Download.State.WAITING_SD && inSDQueueNotWeb)
			{
				return download; // Download matches thread type
			}
			else if (download.state == Download.State.WAITING_WEB && inSDQueueNotWeb == false)
			{
				return download; // Download matches thread type
			}
		}
		
		return null;
	}
	
	/** As SD and Web share the same skeleton we just need this one method for them. */
	private void workerLoop(boolean isSDNotWeb)
	{
		boolean running = true;
		Download download;
		
		// GET INITIAL DOWNLOAD OBJECT (FIRST RUN)
		// Done outside the loop to ensure that there is only one sync block per loop,
		// otherwise the loop has to acquire a lock twice per download.
		synchronized (ImageGetter.this)
		{
			download = nextDownloadNoSyncAndDeleteUnwanted(isSDNotWeb);
			if (download == null)
			{
				running = false;
			}
			else
			{
				download.state = isSDNotWeb ? Download.State.DOWNLOADING_SD : Download.State.DOWNLOADING_WEB;
			}
		}

		// Not thread safe so we need 1 per thread. Null if unavailable.
		final MessageDigest md5Digester = getMD5Digester();
		
		// WORKER LOOP
		// Loop will continue until there are no results left to process (appropriate workerSD or workerWeb will be set to null)
		while (running)
		{
			 // The url is still needed at the bottom of the loop (the download object is overwritten with the "next" one, before "prev" one is written to cache)
			// The sd cache type is still needed at the bottom of the loop
			final String currentDownloadUrl = download.url;
			final SDCacheType currentSDCacheType = download.sdCacheType;
			
			Bitmap image;
			
			// FETCH IMAGE (FROM SD OR WEB)
			// There is no need for this background thread to re-check cache before each download, the items will never "sneak" past the downloader
			if (isSDNotWeb)
			{
				// Reads occur from any of the sd threads, writes occur from any of the web threads
				if (currentSDCacheType == SDCacheType.DISABLED)
				{
					// sd cache is disabled for this download
					image = null;
				}
				else if (md5Digester == null)
				{
					// This shouldn't happen, MD5 should always be available
					image = null;
				}
				else
				{
					// image will be null here, if sd cache is globally disabled
					image = loadBitmapFromSDSnapshotSynched(md5Digester, download.url, download.sdCacheType);
				}
			}
			else
			{
				image = loadBitmapFromUrl(easyHttpConfig, download.url);
			}
			
			// Use this to move the SD write out of the synchronized block
			boolean writeImageToSDCache = false;
			
			// Use this to call recycle out of the synchronized block, only set to true if image is unused
			// If writeImageToSDCache is true as well, write the image before recycling
			boolean recycleImage = false;
			
			// Use this to call the handler only when necessary, out of the synchronized block
			boolean hasAddedPendingCallbacks = false;
			
			synchronized (ImageGetter.this)
			{
				switch (download.state)
				{
					case WAITING_SD:
						throw new RuntimeException("Never Happen Case");
					case WAITING_WEB:
						throw new RuntimeException("Never Happen Case");
					case DOWNLOADING_SD:
					case DOWNLOADING_WEB:
						if (image != null)
						{
							// Add to mem cache, its thread safe. This call can be quite slow in comparison to the other hashmaps
							// Put it in this synch block so that if addUrl is called after a download is completed
							// but before its completion event fires, it doesn't start downloading again
							if (download.memCacheType == MemCacheType.ENABLED)
							{
								memCache.put(download.url, image);
							}
							
							if (isSDNotWeb == false)
							{
								// Add to sd cache
								writeImageToSDCache = currentSDCacheType != SDCacheType.DISABLED;
							}
						}
						
						if (download.interestedParties.size() == 0)
						{
							// Nobody cares any more, Download won't be in downloadByCell.
							downloadByUrl.remove(download.url);
							
							// Image has been added to memcache, do not recycle it here even though it appears unused
						}
						else
						{
							// Loop interested parties, make callback as necessary
							hasAddedPendingCallbacks = true;
							
							for (Callback interestedParty : download.interestedParties)
							{
								PendingCallback replaced = pendingCallbacks.get(interestedParty);
								
								// Error check pendingCallbacks queue
								if (isSDNotWeb)
								{
									// SD download has only ever had a notInMemCache callback by this point.
									// That callback type is never left pending, so anything pending here is an error
									if (replaced != null)
									{
										throw new RuntimeException("Never happen case");
									}
								}
								else
								{
									// Web download should either have no pending callback, or a NOT_IN_SD_CACHE related one
									if (replaced != null && replaced.type != PendingCallback.Type.NOT_IN_SD_CACHE)
									{
										throw new RuntimeException("Never happen case");
									}
								}
								
								// Create the new pending callback with the relevant type
								if (image != null || isSDNotWeb == false)
								{
									// Either SD cache hit, or end of web download
									PendingCallback.Type newType = image != null
											? (isSDNotWeb ? PendingCallback.Type.RESULT_SUCCESS_SD : PendingCallback.Type.RESULT_SUCCESS_WEB)
											: PendingCallback.Type.RESULT_FAIL;
									pendingCallbacks.put(interestedParty, new PendingCallback(interestedParty, newType, download));

									downloadByCell.remove(interestedParty);
								}
								else
								{
									// This is the only case where we carry on with a web download
									PendingCallback.Type newType = PendingCallback.Type.NOT_IN_SD_CACHE;
									pendingCallbacks.put(interestedParty, new PendingCallback(interestedParty, newType, download));
								}
							}
							
							if (image != null || isSDNotWeb == false)
							{
								// Download complete (be it web success/failure or SD cache hit)
								downloadByUrl.remove(download.url);
								
								// The download object will only ever be used again by pendingCallbacks map
								download.interestedParties.clear();

								// Download ok or download failed, depends whether complete is null
								download.complete = image;
								download.state = Download.State.ENDED_OBJ_ONLY_IN_PENDING_CALLBACKS;
							}
							else
							{
								// SD cache miss - Try to get the download from web
								download.state = Download.State.WAITING_WEB;

								// We might need another thread to get this from web now
								startAnotherWebWorkerNeedsSync();
							}
						}
						
						break;
					case DOWNLOADING_SD_CANCEL_WHEN_COMPLETE:
					case DOWNLOADING_WEB_CANCEL_WHEN_COMPLETE:
						// The difference with CANCEL_WHEN_COMPLETE is that the image is thrown away, it will not be added to any cache
						// This is always be the case. Download won't be in downloadByCell.
						if (download.interestedParties.size() == 0)
						{
							downloadByUrl.remove(download.url);
							
							if (image != null)
							{
								recycleImage = true;
							}
						}
						else
						{
							throw new RuntimeException("Never Happen Case");
						}
						
						break;
					case ENDED_OBJ_ONLY_IN_PENDING_CALLBACKS:
						throw new RuntimeException("Never happen case, probably multiple threads accessing the same download object :(");
					default:
						throw new RuntimeException("Never happen case");
				}

				// GET ANOTHER DOWNLOAD TO WORK ON
				download = nextDownloadNoSyncAndDeleteUnwanted(isSDNotWeb);
				if (download == null)
				{
					running = false;
				}
				else
				{
					download.state = isSDNotWeb ? Download.State.DOWNLOADING_SD : Download.State.DOWNLOADING_WEB;
				}
			}
			
			// Keep the handler outside of the synchronized block, its unnecessary (Worst case 2 runnables are enqueued)
			if (hasAddedPendingCallbacks)
			{
				handler.removeCallbacks(completedCallbacksRunnable);
				handler.post(completedCallbacksRunnable);
			}

			// Add to sd cache
			if (writeImageToSDCache && md5Digester != null)
			{
				// Make sure to use the old download url, download was just overwritten with the "next" download object
				writeBitmapToSDCacheSynched(md5Digester, currentDownloadUrl, image, currentSDCacheType);
			}
			
			// Recycle inside synchronized block, after writing to sd card if necessary
			if (recycleImage)
			{
				image.recycle();
				image = null;
			}
		}

		// Do this here instead of in the main synch block to avoid NullPointerException if cache is nulled before bitmap was written to sd cache
		// Whichever thread is the last to shut down has the task of shutting down the SD disc cache.
		synchronized (ImageGetter.this)
		{
			setThisThreadStoppedAndCloseCacheIfLastNeedsSync(isSDNotWeb);
		}
	}

	private void setThisThreadStoppedAndCloseCacheIfLastNeedsSync(boolean isSDNotWeb)
	{
		// This background downloader thread is no longer needed
		if (isSDNotWeb) workersSD.remove(Thread.currentThread());
		else workersWeb.remove(Thread.currentThread());
		
		boolean noMoreSDOrWebThreadsLeft = workersSD.size() + workersWeb.size() == 0;
		
		if (debugThreads) Log.d("ImageGetter", "Thread " + Thread.currentThread().hashCode() + " Shut down, " + workersSD.size() + " SD remain, " + workersWeb.size() + " Web remain, " + downloadByUrl.size() + " downloads remain, " + downloadByCell.size() + " active cells");

		// We don't need to sync the editor itself, as this is always non-null until the very last thread closes (both SD and Web have to be closed)
		// Obviously the last thread needs to close it, so do that here.
		if (noMoreSDOrWebThreadsLeft)
		{
			synchronized (sdCacheSync)
			{
				if (sdCacheState == SDState.OPEN)
				{
					try
					{
						sdCache.close();
					}
					catch (IOException e)
					{
						e.printStackTrace();
					}
					finally
					{
						sdCache = null;
	
						// Let the app try to reopen the sd cache if close failed.
						sdCacheState = SDState.CLOSED;
					}
				}
			}
		}
	}
	
	/** Runnable to be used in the SD downloader thread */
	private final Runnable workerSDRunnable = new Runnable()
	{
    	public void run()
    	{
			workerLoop(true);
    	}
	};

	/** Runnable to be used in the Web downloader thread */
	
	/** Runnable to be used in the Web downloader thread */
	private final Runnable workerWebRunnable = new Runnable()
	{
		public void run()
		{
			workerLoop(false);
		}
	};
	
	/**
	 * Processes all elements in completed, performing callbacks to the registered objects.
	 * Removes processed elements from fetchLastRequestByCell and fetchAnyRequestByUrl.
	 */
    private final Runnable completedCallbacksRunnable = new Runnable()
    {
    	public void run()
    	{
    		// (While I remember why I did it this way...)
    		// Note the potential bug if leaving completed downloads in downloadByUrl:
    		// all background threads might stop because no more waiting downloads are available
    		// e.g. if there is a completed download with 1 interestedParty, then that interestedParty
    		// calls addUrl and it now has 0 interestedParties. Its still left in downloadByUrl queue,
    		// with no background thread to remove it.
    		// addUrl would have to check if interestedParties was reduced to 0 on a completed download,
    		// and clear downloadByUrl as appropriate.
    		
    		synchronized (ImageGetter.this)
    		{
    			// This is performed on the UI thread
    			while (pendingCallbacks.isEmpty() == false)
    			{
    				// Pop one by one the first element in pendingCallbacks.
    				// This accounts for the case the child callback calls cancelCallback or addUrl,
    				// which would modify pendingCallbacks and cause an error if iterating over it.
    				Iterator<java.util.Map.Entry<Callback, PendingCallback>> oneElementIterator = pendingCallbacks.entrySet().iterator();
    				PendingCallback pending = oneElementIterator.next().getValue();
    				oneElementIterator.remove();

    				// Make sure none of these perform 2 callbacks immediately one after the other
    				// (without rechecking pendingCallbacks.isEmpty())
    				switch (pending.type)
    				{
    					case NOT_IN_SD_CACHE:
    						pending.callback.onNotInSDCache();
    						break;
    					case RESULT_FAIL:
    						if (pending.download.complete != null) throw new RuntimeException("Never happen case");
    						pending.callback.onFinishLoadingFailed(pending.download.url);
    						break;
    					case RESULT_SUCCESS_SD:
    					case RESULT_SUCCESS_WEB:
    						if (pending.download.complete == null) throw new RuntimeException("Never happen case");
    						pending.callback.onFinishLoading(
    								pending.download.complete,
    								pending.download.url,
    								pending.type == PendingCallback.Type.RESULT_SUCCESS_SD ? Callback.LoadSource.SD_CACHE : Callback.LoadSource.WEB);
    						break;
						default:
							throw new RuntimeException("Never happen case");
    				}
    				
    			}
    		}
    	}
    };
}
