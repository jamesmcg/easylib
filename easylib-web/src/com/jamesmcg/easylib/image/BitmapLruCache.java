/*
Copyright 2013 James E McGuire

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

BitmapLruCache
*/

package com.jamesmcg.easylib.image;

import android.graphics.Bitmap;
import android.support.v4.util.LruCache;
import android.util.Log;

/**
 * Created by James McGuire
 * 
 * LruCache is an API 12 class, but use android-support-v4.jar for this class. (Need API 17 for trimToSize#int)
 * 
 * An extension to the android support class LruCache, to use proportional amount of free space.
 * Keeps HARD LINKS. You should call clear() if starting another activity on top of the current one, and the images won't be used again soon.
 * 
 * NOTE: Bitmap objects in Android are always included in the heap size calculation, even
 * though they don't actually store pixel data in the heap. Android made sure of this.
 * 
 * Whenever an element is added to the cache the following is checked:
 * If memory usage has increased for things other than the images in this cache,
 * this class will respond by adjusting the maxLimit by the amountOfFreeMemoryToUse
 * proportion.
 * 
 * Essentially will trigger removal of oldest elements to fit 2MB (/ 10MB remaining, 20%)
 * Might allow 16MB at the start (e.g. /80MB remaining, 20%).
 * 
 * 	//						Runtime.getRuntime().maxMemory()	Heap size (as its allocated, an old measurement I can't remember how i got this or if its even correct)
	// Model				Start (KB)		End (KB)
	// HTC trackball 2.3  	32768			32768
	// Galaxy S2 4.1.2		49152			49152				128MB (Always stays the same)
	// Nexus 4 4.3			196608			196608
	// HTC One				196608			196608
	// Galaxy Tab 2			65536			65536
	// Alcatel OneTouch		65536			65536				12.6MB (Happily grows to 51MB after a few screens/small images), I assume it will grow bigger.
 *  
 */
public class BitmapLruCache
{
	private final static String TAG = "BitmapLruCache";
	
	private final boolean debug;
	
	// Memory always left free regardless of amountOfFreeMemoryToUse.
	// We will use a percentage of the rest of the memory.
	private final long bytesAlwaysLeaveFreeOffset;
	private final double amountOfFreeMemoryToUse;

	private final android.support.v4.util.LruCache<String, Bitmap> hardCache;
	
	/** Using this constructor will always keep 1MB spare in the very worse case. */
	public BitmapLruCache(boolean debug, double amountOfFreeMemoryToUse)
	{
		// Default is 1MB always free
		this(debug, amountOfFreeMemoryToUse, 1 * 1024 * 1024);
	}
	
	/** 0.2 (20% of free space) is a recommended value for amountOfFreeMemoryToUse. */
	public BitmapLruCache(boolean debug, double amountOfFreeMemoryToUse, long bytesAlwaysLeaveFreeOffset)
	{
		this.debug = debug;
		
		this.amountOfFreeMemoryToUse = amountOfFreeMemoryToUse;
		this.bytesAlwaysLeaveFreeOffset = bytesAlwaysLeaveFreeOffset;

		// Start with the maximum possible amount of memory so that calls to put() can decrease,
		// but also increase (up to maxSize) the maxSize proportionally by amountOfFreeMemoryToUse.
		long maxMemoryLeaveSomeSpare = Runtime.getRuntime().maxMemory() - bytesAlwaysLeaveFreeOffset;
		int maxUseBytes = (int) ((double) maxMemoryLeaveSomeSpare * amountOfFreeMemoryToUse);
		
		// Calculate current limit
		final int currentLimitBytes;
		{
			final long totalMemory = Runtime.getRuntime().totalMemory();
			final long freeWithinTotalMemory = Runtime.getRuntime().freeMemory();
			final long memoryUsedByRestOfApplication = totalMemory - freeWithinTotalMemory - 0; // cache size is 0 right now so don't include in calculation
			final long freeBytes = maxMemoryLeaveSomeSpare - memoryUsedByRestOfApplication;
			currentLimitBytes = (int) ((double) freeBytes * amountOfFreeMemoryToUse);
		}
		
		Log.e(TAG, "Mem cache will use at most " + (maxUseBytes / 1024) + "KB, initial limit " + (currentLimitBytes / 1024) + "KB");
		
		this.hardCache = new LruCache<String, Bitmap>(maxUseBytes)
		{
			/**
			 * Override this to make the cache limit to memory size, not the default of number of entries
			 * Returns the size of the entry for key and value in user-defined units.
			 * This defines max size as bytes.
			 * An entry's size must not change while it is in the cache.
			 */
			@Override
			protected int sizeOf(String url, Bitmap bitmap)
			{
				// In bytes
				return url.length() + bitmap.getHeight() * bitmap.getRowBytes();
			}
			
			@Override
			protected void entryRemoved(boolean evicted, String url, Bitmap oldValue, Bitmap newValue)
			{
				super.entryRemoved(evicted, url, oldValue, newValue);
				
				if (BitmapLruCache.this.debug && evicted) Log.e(TAG, "Bitmap (" + (sizeOf(url, oldValue) / 1024) + "KB) removed: above memory limit.");
			}
		};
	}
	
	/**
	 * Caches value for key.
	 */
	public void put(String url, Bitmap bitmap)
	{
		// Bitmap has already been allocated so adding now and trimming after is not an issue. We want to keep newest if possible.
		hardCache.put(url, bitmap);

		// The max memory is the maximum memory that the JVM could ever reach.
		// The total memory is the memory that is currently allocated to the JVM. It varies over time.
		// The free memory is the amount of free memory, within the currently allocated chunk (aka total memory)
		final Runtime runtime = Runtime.getRuntime();
		final long maxMemory 			 = runtime.maxMemory();
		final long totalMemory 			 = runtime.totalMemory();
		final long freeWithinTotalMemory = runtime.freeMemory();

		// Calculate the usedBytes excluding entries stored in this cache so we respond
		// only to memory use changes for other activities or causes, not our own fat cache!
		final long maxMemoryLeaveSomeSpare = maxMemory - bytesAlwaysLeaveFreeOffset;
		final long memoryUsedByRestOfApplication = totalMemory - freeWithinTotalMemory - hardCache.size();
		
		// When free memory tightens, reduce our cache proportionately
		final long freeBytes = maxMemoryLeaveSomeSpare - memoryUsedByRestOfApplication;
		final int maxUseBytes = (int) ((double) freeBytes * amountOfFreeMemoryToUse);
		
		if (debug) Log.e(TAG, "Limit to " + (maxUseBytes / 1024) + "KB");
		
		hardCache.trimToSize(maxUseBytes);
	}
	
	/**
	 * Returns the value for url if it exists in the cache.
	 */
	public Bitmap get(String url)
	{
		return hardCache.get(url);
	}
	
	/**
	 * Removes the entry for url if it exists.
	 */
	public Bitmap remove(String url)
	{
		// return hardCache.get(url);
		return hardCache.remove(url);
	}
	
	/**
	 * Remove all entries from the cache.
	 */
	public void clear()
	{
		hardCache.evictAll();
	}
}
