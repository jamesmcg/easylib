#!/bin/bash

# You will need to edit local.properties (with sdk dir) and move it (alongside build.gradle)
# This is only necessary for building manually, outside of an Android Studio project
# IF YOU DONT DO THIS YOU WILL GET "A problem occurred evaluating root project 'EasyLib'.
# > Ambiguous method overloading for method java.io.File#<init>.

cd "$(dirname "$0")"
cd ..
./gradlew aR